
import math
import tensorflow as tf
import tensorflow.keras.layers as layers


class _FourierEmbedding(layers.Layer):

    def __init__(self, input_min, input_max, scales):
        super(_FourierEmbedding, self).__init__()
        self._input_min = input_min
        self._input_max = input_max
        self._scales = scales

    def call(self, input, training=None, mask=None):

        normalised_input = (input - self._input_min) / (self._input_max - self._input_min) * 2. - 1.
        scale_range = tf.range(self._scales, dtype=tf.float32)
        return tf.concat([
            tf.reshape(tf.sin(normalised_input[..., None] * 2. ** scale_range * math.pi), [tf.shape(input)[0], input.shape[1] * self._scales]),
            tf.reshape(tf.cos(normalised_input[..., None] * 2. ** scale_range * math.pi), [tf.shape(input)[0], input.shape[1] * self._scales]),
            normalised_input
        ], axis=-1)


class _ResDense(layers.Layer):

    def __init__(self, activation):
        super(_ResDense, self).__init__()
        self._activation = activation

    def build(self, input_shape):
        self._dense = layers.Dense(input_shape[-1], activation=self._activation)
        self._layer_norm = layers.LayerNormalization()

    def call(self, input, training=None, mask=None):
        return self._layer_norm(input + self._dense(input))


def build(input_count, output_count, hidden_width, hidden_depth, activation, train_input_min, train_input_max, fourier_scales):

    model = tf.keras.Sequential(
        [
            layers.Input([input_count]),
            _FourierEmbedding(train_input_min, train_input_max, fourier_scales),
            layers.Dense(hidden_width, activation=activation)
        ] + [
            _ResDense(activation)
            for _ in range(hidden_depth)
        ] + [layers.Dense(output_count, activation=None)]
    )

    model.Dt1_range = tf.Variable([math.nan, math.nan], trainable=False)
    model.n_range = tf.Variable([math.nan, math.nan], trainable=False)

    return model

