import itertools
import time
import argparse
import numpy as np
import tensorflow as tf

import data
import regressor
import plotting
import simulator


def train_or_restore_model(model, dataset, args):

    optimizer = tf.keras.optimizers.Adam(learning_rate=1.e-3)

    model.compile(
        optimizer=optimizer,
        loss='mae',
        metrics=['mae', tf.keras.metrics.MeanAbsolutePercentageError()]
    )

    early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=args.patience_epochs)
    save_checkpoints = tf.keras.callbacks.ModelCheckpoint(args.checkpoint_path + '/ckpt', save_weights_only=True, save_best_only=True, monitor='val_loss')

    if args.restore:
        model.load_weights(args.checkpoint_path + '/ckpt')
        assert tf.reduce_all(model.Dt1_range == [args.Dt1_min, args.Dt1_max])
        assert tf.reduce_all(model.n_range == [args.n_min, args.n_max])
    else:
        model.Dt1_range.assign([args.Dt1_min, args.Dt1_max])
        model.n_range.assign([args.n_min, args.n_max])
        history = model.fit(
            dataset.train_data,
            validation_data=dataset.val_data,
            epochs=args.max_epochs,
            validation_steps=1000,
            verbose=1,
            callbacks=[early_stopping, save_checkpoints]
        )


def evaluate(model, dataset, args):

    abs_errors = []
    rel_errors = []
    for inputs, test_outputs_sim in dataset.test_data:

        test_outputs_pred = model.predict_on_batch(inputs)
        abs_errors.append(tf.abs(test_outputs_sim - test_outputs_pred).numpy())
        rel_errors.append(tf.abs((test_outputs_sim - test_outputs_pred) / (test_outputs_sim + 1.e-12)).numpy())

    abs_errors = np.concatenate(abs_errors, axis=0)
    rel_errors = np.concatenate(rel_errors, axis=0)
    test_maes = np.mean(abs_errors, axis=0)
    test_medaes = np.median(abs_errors, axis=0)
    test_mres = np.mean(rel_errors, axis=0)
    test_medres = np.median(rel_errors, axis=0)

    print('statistics on test set:')
    if args.n_as_output:
        print(f' mean absolute errors: n = {test_maes[0]:.4f}, nu = {test_maes[1]:.4f}')
        print(f' median absolute errors: n = {test_medaes[0]:.4f}, nu = {test_medaes[1]:.4f}')
        print(f' mean relative errors: n = {test_mres[0]:.3f}, nu = {test_mres[1]:.3f}')
        print(f' median relative errors: n = {test_medres[0]:.3f}, nu = {test_medres[1]:.3f}')
    else:
        print(f' mean absolute error in nu = {test_maes[0]:.6f}')
        print(f' median absolute error in nu = {test_medaes[0]:.6f}')
        print(f' mean relative error in nu = {test_mres[0]:.6f}')
        print(f' median relative error in nu = {test_medres[0]:.6f}')


def evaluate_hi_res(model, dataset, args):

    # This evaluates at the plot level (rather than the shuffled test-set above), but still applies the same
    # range filtering as in the data preprocessor, so statistics should be comparable
    # It computes two sets of statistics -- first with the simulator output from the dataset, and second
    # re-simulated, assuming the constants have been set for a higher-resolution simulation (more ks) on
    # the same set of mu and Dt1, e.g. setting ND = 1000, kmax = 0.2

    abs_errors_hi = []
    rel_errors_hi = []
    abs_errors_lo = []
    rel_errors_lo = []
    abs_errors_lo_hi = []
    rel_errors_lo_hi = []
    for test_image_params, test_image_true_data_lo_res in dataset.unnormalised_test_data_by_image():

        # test_image_params is g2, g3, g4, Dt2; each row of test_image_true_data is Dt1, mu, n, Cp

        num_points = test_image_true_data_lo_res.shape[0]
        test_image_inputs = np.concatenate([
            np.tile(test_image_params.astype(np.float32), [num_points, 1]),
            test_image_true_data_lo_res[:, ::2],
            args.eta_max * np.ones([num_points, 1])
        ], axis=1)  # each row is g2, g3, g4, Dt2, Dt1, n, eta

        test_image_pred_outputs = model.predict_on_batch(test_image_inputs)

        test_image_true_data_hi_res = simulator.run(test_image_params)
        assert test_image_true_data_hi_res.shape[0] == num_points

        true_in_train_range = np.logical_and(
            np.logical_not(np.all(np.abs(test_image_true_data_hi_res[:, 2:]) < 1.e-2, axis=1)),
            np.logical_and(
                np.logical_and(args.Dt1_min <= test_image_true_data_hi_res[:, 0], test_image_true_data_hi_res[:, 0] <= args.Dt1_max),
                np.logical_and(args.n_min <= test_image_true_data_hi_res[:, 2], test_image_true_data_hi_res[:, 2] <= args.n_max)
            )
        )
        filtered_true_nu_lo_res = test_image_true_data_lo_res[true_in_train_range][:, -1:]
        filtered_true_nu_hi_res = test_image_true_data_hi_res[true_in_train_range][:, -1:]
        filtered_pred = test_image_pred_outputs[true_in_train_range]

        abs_errors_lo.append(np.abs(filtered_true_nu_lo_res - filtered_pred))
        rel_errors_lo.append(np.abs((filtered_true_nu_lo_res - filtered_pred) / (filtered_true_nu_lo_res + 1.e-12)))
        abs_errors_hi.append(np.abs(filtered_true_nu_hi_res - filtered_pred))
        rel_errors_hi.append(np.abs((filtered_true_nu_hi_res - filtered_pred) / (filtered_true_nu_hi_res + 1.e-12)))
        abs_errors_lo_hi.append(np.abs(filtered_true_nu_hi_res - filtered_true_nu_lo_res))
        rel_errors_lo_hi.append(np.abs((filtered_true_nu_hi_res - filtered_true_nu_lo_res) / (filtered_true_nu_hi_res + 1.e-12)))

        print(f'after {len(abs_errors_lo)} plots...')
        print(' DNN vs low-resolution simulations:')
        print(f'  mean absolute error in nu = {np.concatenate(abs_errors_lo, axis=0).mean():.6f}')
        print(f'  mean relative error in nu = {np.concatenate(rel_errors_lo, axis=0).mean():.6f}')
        print(' DNN vs high-resolution simulations:')
        print(f'  mean absolute error in nu = {np.concatenate(abs_errors_hi, axis=0).mean():.6f}')
        print(f'  mean relative error in nu = {np.concatenate(rel_errors_hi, axis=0).mean():.6f}')
        print(' low vs high-resolution simulations:')
        print(f'  mean absolute error in nu = {np.concatenate(abs_errors_lo_hi, axis=0).mean():.6f}')
        print(f'  mean relative error in nu = {np.concatenate(rel_errors_lo_hi, axis=0).mean():.6f}')


def save_comparisons(model, dataset, args):

    plotter = plotting.Plotter(args)

    count = 100
    for test_image_params, test_image_true_data in itertools.islice(dataset.unnormalised_test_data_by_image(), count):

        # test_image_params is g2, g3, g4, Dt2; each row of test_image_true_data is Dt1, mu, n, Cp

        if args.n_as_output:
            # In this case, we 'borrow' the (gridded) Dt1 and mu values from the test input, but ignore the outputs!
            # We remove those points whose true n falls outside the model's trained range, as it is not expected to work here
            test_image_inputs = np.concatenate([
                np.tile(test_image_params.astype(np.float32), [test_image_true_data.shape[0], 1]),
                test_image_true_data.astype(np.float32)[:, :2],
                args.eta_max * np.ones([test_image_true_data.shape[0], 1])
            ], axis=1)
            test_image_inputs = test_image_inputs[np.logical_and(model.n_range[0] <= test_image_true_data[:, 2], test_image_true_data[:, 2] <= model.n_range[1])]
        else:
            # In this case, we evaluate on exactly the points we want to plot
            grid = plotter.grid
            test_image_inputs = np.concatenate([
                np.tile(test_image_params.astype(np.float32), [grid.yi.size, 1]),
                np.reshape(np.stack([grid.xi, grid.yi], axis=-1), [-1, 2]),
                args.eta_max * np.ones([grid.yi.size, 1])
            ], axis=1)  # each row is g2, g3, g4, Dt2, Dt1, n, eta

        start_time = time.time()
        test_image_pred_outputs = model.predict(test_image_inputs)
        print(f'took {time.time() - start_time:.3f}s')

        if args.n_as_output:
            test_image_pred_data = np.concatenate([test_image_inputs[:, 4:], test_image_pred_outputs], axis=1)
        else:
            # Insert fake mu values to satisfy plotting function
            test_image_pred_data = np.concatenate([test_image_inputs[:, 4:5], np.zeros_like(test_image_inputs[:, :1]), test_image_inputs[:, 5:6], test_image_pred_outputs], axis=1)

        z_true = plotter.interpolate_onto_grid(test_image_true_data)
        z_pred = plotter.interpolate_onto_grid(test_image_pred_data)
        plotter.plot_comparison(z_true, z_pred, test_image_params, save_path=args.checkpoint_path + '/comparisons')


def generate_fig2(model, dataset, args):

    plotter = plotting.Plotter(args)

    assert not args.n_as_output

    z_trues = []
    z_preds = []
    params = []
    count = 3
    for test_image_params, test_image_true_data_lo_res in itertools.islice(dataset.unnormalised_test_data_by_image(), count):

        # test_image_params is g2, g3, g4, Dt2; each row of test_image_true_data is Dt1, mu, n, Cp

        grid = plotter.grid
        test_image_inputs = np.concatenate([
            np.tile(test_image_params.astype(np.float32), [grid.yi.size, 1]),
            np.reshape(np.stack([grid.xi, grid.yi], axis=-1), [-1, 2]),
            args.eta_max * np.ones([grid.yi.size, 1])
        ], axis=1)  # each row is g2, g3, g4, Dt2, Dt1, n, eta

        test_image_pred_outputs = model.predict_on_batch(test_image_inputs)

        test_image_true_data = simulator.run(test_image_params)

        # Insert fake mu values to satisfy plotting function
        test_image_pred_data = np.concatenate([test_image_inputs[:, 4:5], np.zeros_like(test_image_inputs[:, :1]), test_image_inputs[:, 5:6], test_image_pred_outputs], axis=1)

        z_trues.append(plotter.interpolate_onto_grid(test_image_true_data))
        z_preds.append(plotter.interpolate_onto_grid(test_image_pred_data))
        params.append(test_image_params)

    plotter.plot_fig2(z_trues, z_preds, params, args.checkpoint_path + '/fig2.pdf')


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--data-path', type=str, default='./data/ABC/2020-08-17')
    parser.add_argument('--checkpoint-path', type=str, default='./output/ABC/latest')
    parser.add_argument('--restore', action='store_true')
    parser.add_argument('--plots-only', action='store_true')
    parser.add_argument('--seed', type=int, default=0)
    parser.add_argument('--max-epochs', type=int, default=100)
    parser.add_argument('--patience-epochs', type=int, default=10)
    parser.add_argument('--train-batch-size', type=int, default=512)
    parser.add_argument('--test-batch-size', type=int, default=1000000)
    parser.add_argument('--val-fraction', type=float, default=0.1)
    parser.add_argument('--test-fraction', type=float, default=0.15)
    parser.add_argument('--n-as-output', action='store_true')
    parser.add_argument('--Dt1-min', type=float, default=0.)
    parser.add_argument('--Dt1-max', type=float, default=75.)
    parser.add_argument('--n-min', type=float, default=-4.)
    parser.add_argument('--n-max', type=float, default=4.)
    parser.add_argument('--eta-min', type=float, default=5.e1)
    parser.add_argument('--eta-max', type=float, default=5.e3)
    args = parser.parse_args()

    if args.plots_only:
        assert args.restore  # as we required shuffled preprocessed data for training

    if len(tf.config.get_visible_devices('GPU')) > 0:
        tf.config.experimental.set_memory_growth(tf.config.get_visible_devices('GPU')[0], True)
    # tf.config.threading.set_inter_op_parallelism_threads(1)
    # tf.config.threading.set_intra_op_parallelism_threads(4)  # set this to limit number of CPU threads used

    np.random.seed(args.seed)
    tf.random.set_seed(args.seed)

    dataset = data.DatasetV2(args, plots_only=args.plots_only)

    min_inputs = np.asarray([dataset.g2_range[0], dataset.g3_range[0], dataset.g4_range[0], dataset.Dt2_range[0], args.Dt1_min, args.n_min, args.eta_min])
    max_inputs = np.asarray([dataset.g2_range[1], dataset.g3_range[1], dataset.g4_range[1], dataset.Dt2_range[1], args.Dt1_max, args.n_max, args.eta_max])
    model = regressor.build(
        input_count=7, output_count=2 if args.n_as_output else 1,
        hidden_width=512, hidden_depth=5,
        activation=tf.nn.elu,
        train_input_min=min_inputs, train_input_max=max_inputs,
        fourier_scales=4
    )

    train_or_restore_model(model, dataset, args)

    evaluate_hi_res(model, dataset, args)

    generate_fig2(model, dataset, args)

    if not args.plots_only:
        evaluate(model, dataset, args)

    save_comparisons(model, dataset, args)


if __name__ == '__main__':

    main()

