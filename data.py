
import os
import pickle
import itertools
import functools
import numpy as np
import scipy.interpolate
import tensorflow as tf
from tqdm import tqdm
from dataclasses import dataclass


class DatasetV1:

    g2_range = np.arange(-25.0, 0.0, 4.0)
    g3_range = np.arange(-340.0, -238.0, 10.0)
    g4_range = np.arange(-181.0, -100.0, 10.0)
    Dt2_range = np.arange(0.0, 11.0, 2.0)

    def __init__(self, args):

        self.raw_data = _load_or_import_data(args.data_path)
        (
            self.train_data, self.val_data,
            (self.test_inputs_flat, self.test_outputs_flat),
            self.unnormalised_test_data_by_image,
            self.train_input_min, self.train_input_max,
            self.train_output_mean, self.train_output_std
        ) = _preprocess_data(self.raw_data, args)

    def get_raw_nearest_parameters(self, g2, g3, g4, Dt2):
        # This returns a tuple (parameters, points)
        parameters = np.asarray([g2, g3, g4, Dt2])
        return min(self.raw_data, key=lambda parameters_and_points: np.linalg.norm(parameters_and_points[0] - parameters))


class DatasetV2:

    @dataclass
    class Plot:
        g234Dt2 : np.ndarray
        mu_minus_mu0 : np.ndarray
        input_cols : np.ndarray
        output_cols : np.ndarray

    def __init__(self, args, force_preprocess=False, plots_only=False):

        self._g234Dt2 = self.load_params(args.data_path)
        min_g234Dt2 = self._g234Dt2.min(axis=0)
        max_g234Dt2 = self._g234Dt2.max(axis=0)
        self.g2_range, self.g3_range, self.g4_range, self.Dt2_range = np.transpose([min_g234Dt2, max_g234Dt2])

        if force_preprocess:
            self._preprocess(args)

        with open(os.path.join(args.data_path, 'preprocessed', 'indices.pkl'), 'rb') as indices_file:
            self._train_indices, self._val_indices, self._test_indices = pickle.load(indices_file)

        if not plots_only:

            assert not args.n_as_output  # else the columns are different from what we assume!

            def load_tfrecord(filename):
                return tf.data.TFRecordDataset(filename).map(lambda example: tf.io.parse_single_example(
                    example, {'x': tf.io.FixedLenFeature([8], dtype=tf.float32)}
                )['x'])

            def get_raw_split_dataset(split, batch_size):
                return tf.data.Dataset.list_files(os.path.join(args.data_path, 'preprocessed', split, '*.tfrecord')) \
                    .interleave(load_tfrecord, cycle_length=4, block_length=8, num_parallel_calls=tf.data.AUTOTUNE) \
                    .batch(batch_size)  # iib, g234Dt2 / mu_minus_mu0 / Dt1 / n / DOS

            @tf.autograph.experimental.do_not_convert
            def split_and_apply_eta(all_cols, random_or_large):
                # all_cols :: iib, g234Dt2 / mu-mu0 / Dt1 / n / DOS
                param_cols, mu_minus_mu0, coord_cols, output_cols = tf.split(all_cols, [4, 1, 2, 1], axis=-1)
                if random_or_large == 'random':
                    # This uses numpy_function to ensure we get different randomness each epoch
                    eta = tf.numpy_function(
                        lambda shape: np.exp(np.random.uniform(np.log(args.eta_min), np.log(args.eta_max), shape).astype(np.float32)),
                        [tf.shape(mu_minus_mu0)],
                        tf.float32
                    )
                    eta.set_shape(mu_minus_mu0.get_shape())
                elif random_or_large == 'large':
                    eta = tf.ones_like(mu_minus_mu0) * args.eta_max
                else:
                    assert False
                n_sim = coord_cols[:, 1:]
                n_exp = n_sim + mu_minus_mu0 / eta
                return (
                    tf.concat([param_cols, coord_cols[:, :1], n_exp, eta], axis=1),  # iib, g234Dt2 / Dt1 / n_exp / eta
                    output_cols
                )

            self.train_data = get_raw_split_dataset('train', args.train_batch_size).map(functools.partial(split_and_apply_eta, random_or_large='random'), num_parallel_calls=tf.data.AUTOTUNE)
            self.val_data = get_raw_split_dataset('val', args.test_batch_size).map(functools.partial(split_and_apply_eta, random_or_large='large'), num_parallel_calls=tf.data.AUTOTUNE)
            self.test_data = get_raw_split_dataset('test', args.test_batch_size).map(functools.partial(split_and_apply_eta, random_or_large='large'), num_parallel_calls=tf.data.AUTOTUNE)

        def generate_unnormalised_test_plots():
            for idx in self._test_indices:
                dataset = np.load(os.path.join(args.data_path, f'DOSND{int(idx)}.npy')).astype(np.float32)
                yield self._g234Dt2[idx], dataset.reshape(-1, 4)
        self.unnormalised_test_data_by_image = generate_unnormalised_test_plots

    def _preprocess(self, args):

        def load_plot(idx):
            dataset = np.load(os.path.join(args.data_path, f'DOSND{int(idx)}.npy')).astype(np.float32)  # Dt1-index, mu-index, Dt1/mu/n/DOS
            dataset = dataset.reshape(-1, 4)  # point-index, Dt1/mu/n/DOS
            dataset = dataset[np.logical_not(np.all(np.abs(dataset[:, 2:]) < 1.e-2, axis=1))]  # i.e. remove rows where both outputs are predicted close to zero
            dataset = dataset[np.logical_and(args.Dt1_min <= dataset[:, 0], dataset[:, 0] <= args.Dt1_max)]
            dataset = dataset[np.logical_and(args.n_min <= dataset[:, 2], dataset[:, 2] <= args.n_max)]
            if args.n_as_output:  # i.e. mu is an input, and we are predicting both n and Cp
                input_cols = dataset[:, [0, 1]]
                output_cols = dataset[:, 2:]
            else:  # i.e. mu is latent; we are predict Cp directly from n
                input_cols = dataset[:, [0, 2]]
                output_cols = dataset[:, 3:]
            mu0 = _get_mu0(dataset)
            return self.Plot(self._g234Dt2[idx], dataset[:, 1:2] - mu0, input_cols, output_cols)

        def write_for_split(split_name, split_indices):
            num_plots_per_block = 1000
            for block_start in range(0, len(split_indices), num_plots_per_block):
                plots = [
                    load_plot(split_indices[plot_index_in_split])
                    for plot_index_in_split in tqdm(
                        range(block_start, min(block_start + num_plots_per_block, len(split_indices))),
                        desc=f'loading block {block_start // num_plots_per_block}'
                    )
                ]
                plot_and_point_indices = np.concatenate([
                    np.stack([
                        np.broadcast_to(np.int32(plot_index), [len(plot.input_cols)]),
                        np.arange(len(plot.input_cols), dtype=np.int32)
                    ], axis=1)
                    for plot_index, plot in enumerate(plots)
                ], axis=0)
                print(f'block contains {len(plot_and_point_indices)} points')
                np.random.shuffle(plot_and_point_indices)
                with tf.io.TFRecordWriter(os.path.join(args.data_path, 'preprocessed', split_name, f'{block_start:06}.tfrecord')) as writer:
                    for plot_index, point_index in tqdm(plot_and_point_indices, desc=f'saving block {block_start // num_plots_per_block}'):
                        plot = plots[plot_index]
                        writer.write(tf.train.Example(features=tf.train.Features(feature={
                            'x': tf.train.Feature(float_list=tf.train.FloatList(value=np.concatenate([
                                plot.g234Dt2, plot.mu_minus_mu0[point_index], plot.input_cols[point_index], plot.output_cols[point_index]
                            ]))),
                        })).SerializeToString())

        permutation = np.random.permutation(len(self._g234Dt2))  # this will index files/plots
        first_val_index = int(len(permutation) * (1. - args.val_fraction - args.test_fraction))
        first_test_index = int(len(permutation) * (1. - args.test_fraction))

        train_indices = permutation[:first_val_index]
        val_indices = permutation[first_val_index : first_test_index]
        test_indices = permutation[first_test_index:]

        with open(os.path.join(args.data_path, 'preprocessed', 'indices.pkl'), 'wb') as indices_file:
            pickle.dump((train_indices, val_indices, test_indices), indices_file)

        write_for_split('train', train_indices)
        write_for_split('val', val_indices)
        write_for_split('test', test_indices)

    @staticmethod
    def load_params(data_path):
        params = np.genfromtxt(
            os.path.join(data_path, 'param.csv'),
            names=['index', 'g2', 'g3', 'g4', 'Dt2'],
            skip_header=1, dtype=None
        )
        assert (params['index'] == range(len(params))).all()  # i.e. make sure the indices equal the row numbers, so we can ignore them
        return np.stack([params['g2'], params['g3'], params['g4'], params['Dt2']], axis=1).astype(np.float32)  # plot-index, g2/g3/g4/Dt2


def load_raw(path, g2, g3, g4, Dt2):

    def format_value(x, name):
        return name + ('m' if x < 0 else '') + f'{abs(x):.1f}'

    return np.loadtxt(
        path + '/DOSND500kmax0.15T0.025dt35.0'
        + format_value(g2, 'g2')
        + format_value(g3, 'g3')
        + format_value(g4, 'g4')
        + format_value(Dt2, 'Dt2')
        + '.dat'
    )


def _import_data(path):

    return [
        (
            np.asarray([g2, g3, g4, Dt2]),
            load_raw(path, g2, g3, g4, Dt2)
        )
        for g2, g3, g4, Dt2 in tqdm(list(itertools.product(DatasetV1.g2_range, DatasetV1.g3_range, DatasetV1.g4_range, DatasetV1.Dt2_range)))
    ]


def _load_or_import_data(path):

    cache_filename = path + '/cache.pickle'
    try:
        with open(cache_filename, 'rb') as f:
            raw_data = pickle.load(f)
    except FileNotFoundError:
        raw_data = _import_data(path)
        with open(cache_filename, 'wb') as f:
            pickle.dump(raw_data, f)
    return raw_data


def _get_mu0(Dt1_mu_n_dos):
    # Find mu(Dt1=0, n=0) for the given plot; note that we assume Delta1 = 0 is included in the parameter grid
    # We disregard simulated points with *exactly* n = 0, and instead extrapolate from nearby ones
    Dt1, mu, n, Cp = map(np.squeeze, np.split(Dt1_mu_n_dos, 4, axis=1))
    valid_points = np.logical_and(Dt1 == 0., n != 0.)
    if np.count_nonzero(valid_points) == 1:
        return mu[valid_points][0]  # ** reasonable?
    else:
        interpolated = scipy.interpolate.interp1d(n[valid_points], mu[valid_points], fill_value='extrapolate')(0.)
        if np.isinf(interpolated):  # occurs when two values of mu yield the same n
            return mu[valid_points][np.argmin(np.abs(n[valid_points]))]
        else:
            return interpolated


def _preprocess_data(raw_data, args):

    # raw_data :: List[Tuple[NdArray[4], NdArray[24000, 4]]; each element of the list correspond to one plot

    permutation = np.random.permutation(len(raw_data))  # this will index files/plots
    first_val_index = int(len(raw_data) * (1. - args.val_fraction - args.test_fraction))
    first_test_index = int(len(raw_data) * (1. - args.test_fraction))

    def to_flat_split(indices_for_split, normalisation_mean, normalisation_std):
        dataset = np.concatenate([
            np.concatenate([
                np.tile(
                    np.concatenate([
                        raw_data[index][0],
                        _get_mu0(raw_data[index][1])[None]
                    ], axis=0)[None].astype(np.float32),
                    [raw_data[index][1].shape[0], 1]
                ),
                raw_data[index][1].astype(np.float32)
            ], axis=1)
            for index in indices_for_split
        ], axis=0)  # each row is g2, g3, g4, Dt2, mu0, Dt1, mu, n, amplitude
        # dataset = dataset[np.logical_not(np.all(dataset[:, 7:] == 0., axis=1))]  # i.e. remove rows where both outputs are predicted exactly zero
        dataset = dataset[np.logical_not(np.all(np.abs(dataset[:, 7:]) < 1.e-2, axis=1))]  # i.e. remove rows where both outputs are predicted close to zero
        dataset = dataset[np.logical_and(args.Dt1_min <= dataset[:, 5], dataset[:, 5] <= args.Dt1_max)]
        dataset = dataset[np.logical_and(args.n_min <= dataset[:, 7], dataset[:, 7] <= args.n_max)]
        dataset = dataset[np.random.permutation(len(dataset))]  # to disperse rows from each file
        if args.n_as_output:  # i.e. mu is an input, and we are predicting both n and Cp
            input_cols = dataset[:, [0, 1, 2, 3, 5, 6]]
            output_cols = dataset[:, 7:]
        else:  # i.e. mu is latent; we are predict Cp directly from n
            input_cols = dataset[:, [0, 1, 2, 3, 5, 7]]
            output_cols = dataset[:, 8:]
        output_cols = (output_cols - normalisation_mean) / normalisation_std
        mu_minus_mu0_col = dataset[:, 6:7] - dataset[:, 4:5]
        return input_cols, output_cols, mu_minus_mu0_col

    def to_tf_dataset(input_cols, output_cols, mu0_col):
        return tf.data.Dataset \
            .from_tensor_slices((input_cols, output_cols, mu0_col)) \
            .shuffle(10000)

    normalisation_count = 500  # this is a number of files/plots
    assert normalisation_count <= first_val_index  # else we 'cheat' and use validation points for normalisation
    normalisation_subset_inputs, normalisation_subset_outputs, _ = to_flat_split(permutation[:normalisation_count], 0., 1.)
    train_output_mean = np.mean(normalisation_subset_outputs, axis=0)
    train_output_std = np.std(normalisation_subset_outputs, axis=0)
    train_input_min = np.min(normalisation_subset_inputs, axis=0)
    train_input_max = np.max(normalisation_subset_inputs, axis=0)

    @tf.autograph.experimental.do_not_convert
    def apply_eta(input_cols, output_cols, mu_minus_mu0, random_or_large):
        assert not args.n_as_output
        # input_cols :: iib, g2/g3/g4/Dt2/Dt1/n
        if random_or_large == 'random':
            # This uses numpy_function to ensure we get different randomness each epoch
            eta = tf.numpy_function(
                lambda shape: np.exp(np.random.uniform(np.log(args.eta_min), np.log(args.eta_max), shape).astype(np.float32)),
                [tf.shape(mu_minus_mu0)],
                tf.float32
            )
            eta.set_shape(mu_minus_mu0.get_shape())
        elif random_or_large == 'large':
            eta = tf.ones_like(mu_minus_mu0) * args.eta_max
        else:
            assert False
        n_sim = input_cols[:, 5:]
        n_exp = n_sim + mu_minus_mu0 / eta
        return tf.concat([input_cols[:, :5], n_exp, eta], axis=1), output_cols

    train_cols = to_flat_split(permutation[:first_val_index], train_output_mean, train_output_std)
    val_cols = to_flat_split(permutation[first_val_index : first_test_index], train_output_mean, train_output_std)

    return (
        to_tf_dataset(*train_cols).batch(args.train_batch_size).map(functools.partial(apply_eta, random_or_large='random'), num_parallel_calls=4,deterministic=False).prefetch(32),
        to_tf_dataset(*val_cols).batch(args.test_batch_size).map(functools.partial(apply_eta, random_or_large='large'), num_parallel_calls=4,deterministic=False).prefetch(32),
        to_flat_split(permutation[first_test_index:], train_output_mean, train_output_std)[:2],
        [raw_data[index] for index in (permutation[first_test_index:])],
        np.concatenate([train_input_min, [args.eta_min]]), np.concatenate([train_input_max, [args.eta_max]]),
        train_output_mean, train_output_std
    )

