
import argparse
import itertools
import numpy as np
import scipy.optimize
import tensorflow as tf
from tqdm import tqdm

import data
import regressor
import plotting
import simulator


@tf.function
def _loss_and_grad_impl(model, parameters, Dt1s_and_ns, eta, true_densities):

    with tf.GradientTape() as tape:
        tape.watch(parameters)
        inputs = tf.concat([tf.broadcast_to(parameters, [Dt1s_and_ns.shape[0], parameters.shape[0]]), Dt1s_and_ns, tf.broadcast_to(eta, [Dt1s_and_ns.shape[0], 1])], axis=1)
        predicted_densities = tf.squeeze(model(inputs), axis=1)
        recon_loss = tf.reduce_mean(tf.square(predicted_densities - true_densities))

    return recon_loss, tape.gradient(recon_loss, parameters)


def get_loss_and_grad(parameters, model, true_densities, Dt1s_and_ns, eta):

    parameters = tf.convert_to_tensor(parameters.astype(np.float32))
    recon_loss, grad = _loss_and_grad_impl(model, parameters, Dt1s_and_ns, eta, true_densities)
    return recon_loss.numpy().astype(np.float64), grad.numpy().astype(np.float64)


def predict_parameters(true_image_data, model, min_parameters, max_parameters, args):

    # parameters to be predicted are  g2, g3, g4, Dt2; each row of true_image_data is Dt1, mu, n, density of states (dn/dmu = nu)

    # The following match the filters we perform when preprocessing train/val data
    true_image_data = true_image_data[np.logical_not(np.all(np.abs(true_image_data[:, 2:]) < 1.e-2, axis=1))]
    true_image_data = true_image_data[np.logical_and(args.Dt1_min <= true_image_data[:, 0], true_image_data[:, 0] <= args.Dt1_max)]
    true_image_data = true_image_data[np.logical_and(args.n_min <= true_image_data[:, 2], true_image_data[:, 2] <= args.n_max)]

    best_loss = np.inf
    best_parameters = None
    for attempt in tqdm(range(10)):
        result = scipy.optimize.minimize(
            get_loss_and_grad,
            x0=np.random.uniform(min_parameters, max_parameters),
            args=(
                model,
                tf.constant(true_image_data[:, 3].astype(np.float32)),
                tf.constant(true_image_data[:, 0::2].astype(np.float32)),
                tf.constant(model.eta_range[1])  # fix eta large, as the simulation effectively assumes it's infinite
            ),
            method='trust-constr',
            jac=True,
            bounds=np.stack([min_parameters, max_parameters], axis=1)
        )

        if result.success:
            # print(f'optimisation #{attempt} converged in {result.nit} iterations ({result.nfev} evaluations)')
            if result.fun < best_loss:
                best_loss = result.fun
                best_parameters = result.x

    assert best_parameters is not None  # only happens if we failed every attempt!
    return best_parameters


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--data-path', type=str, default='./data/ABC/2020-08-17')
    parser.add_argument('--checkpoint-path', type=str, default='./output/ABC/latest')
    parser.add_argument('--count', type=int, default=100)
    parser.add_argument('--seed', type=int, default=0)
    parser.add_argument('--val-fraction', type=float, default=0.1)
    parser.add_argument('--test-fraction', type=float, default=0.15)
    parser.add_argument('--n-as-output', action='store_true')
    parser.add_argument('--Dt1-min', type=float, default=0.)  # these define the range the model was trained on (and hence gives valid output for!)
    parser.add_argument('--Dt1-max', type=float, default=75.)
    parser.add_argument('--n-min', type=float, default=-4.)
    parser.add_argument('--n-max', type=float, default=4.)
    parser.add_argument('--eta-min', type=float, default=5.e1)
    parser.add_argument('--eta-max', type=float, default=5.e3)
    args = parser.parse_args()

    args.train_batch_size = args.test_batch_size = 1  # required for dataset, but we always treat a whole plot as a batch

    if len(tf.config.get_visible_devices('GPU')) > 0:
        tf.config.experimental.set_memory_growth(tf.config.get_visible_devices('GPU')[0], True)

    np.random.seed(args.seed)
    tf.random.set_seed(args.seed)

    dataset = data.DatasetV2(args, plots_only=True)

    min_parameters = np.asarray([dataset.g2_range[0], dataset.g3_range[0], dataset.g4_range[0], dataset.Dt2_range[0]])
    max_parameters = np.asarray([dataset.g2_range[1], dataset.g3_range[1], dataset.g4_range[1], dataset.Dt2_range[1]])
    model = regressor.build(
        input_count=7, output_count=2 if args.n_as_output else 1,
        hidden_width=512, hidden_depth=5,
        activation=tf.nn.elu,
        train_input_min=np.concatenate([min_parameters, [args.Dt1_min, args.n_min, args.eta_min]]), train_input_max=np.concatenate([max_parameters, [args.Dt1_max, args.n_max, args.eta_max]]),
        fourier_scales=4
    )
    model.load_weights(args.checkpoint_path + '/ckpt').expect_partial()
    assert tf.reduce_all(model.Dt1_range == [args.Dt1_min, args.Dt1_max])
    assert tf.reduce_all(model.n_range == [args.n_min, args.n_max])
    model.eta_range = [args.eta_min, args.eta_max]  # ** should save this in the model when training

    assert not args.n_as_output

    plotter = plotting.Plotter(args)

    midrange_params = (np.float32(max_parameters) + min_parameters) / 2.

    abs_errors = []
    centered_rel_errors = []
    for plot_index, (test_image_true_params, test_image_true_data) in enumerate(itertools.islice(dataset.unnormalised_test_data_by_image(), args.count)):

        print(f'plot {plot_index + 1} / {args.count}')

        # test_image_params is g2, g3, g4, Dt2; each row of test_image_true_data is Dt1, mu, n, nu

        test_image_pred_params = predict_parameters(test_image_true_data, model, min_parameters, max_parameters, args)
        print(f'  true parameters: {test_image_true_params}')
        print(f'  predicted parameters: {np.around(test_image_pred_params, 2)}')

        abs_errors.append(np.abs(test_image_pred_params - test_image_true_params))
        centered_rel_errors.append(np.abs(test_image_pred_params - test_image_true_params) / np.maximum(1.e-2 * np.abs(midrange_params), np.abs(test_image_true_params - midrange_params)))

        grid = plotter.grid
        test_image_recon_inputs = np.concatenate([
            np.tile(test_image_pred_params.astype(np.float32), [grid.yi.size, 1]),
            np.reshape(np.stack([grid.xi, grid.yi], axis=-1), [-1, 2])
        ], axis=1)  # each row is g2, g3, g4, Dt2, Dt1, n

        test_image_recon_inputs_with_eta = np.concatenate([test_image_recon_inputs, np.broadcast_to(model.eta_range[1], [test_image_recon_inputs.shape[0], 1])], axis=1)
        test_image_recon_outputs = model.predict(test_image_recon_inputs_with_eta)

        # Insert fake mu values to satisfy plotting function
        test_image_recon_data = np.concatenate([test_image_recon_inputs[:, 4:5], np.zeros_like(test_image_recon_inputs[:, 5:]), test_image_recon_inputs[:, 5:], test_image_recon_outputs], axis=1)

        # test_image_sim_params_near_pred, test_image_sim_data_near_pred = dataset.get_raw_nearest_parameters(*test_image_pred_params)
        test_image_sim_data_at_pred = simulator.run(test_image_pred_params)

        z_true = plotter.interpolate_onto_grid(test_image_true_data)
        # z_pred = plotter.interpolate_onto_grid(test_image_recon_data)
        # z_sim_near_pred = plotter.interpolate_onto_grid(test_image_sim_data_near_pred)
        z_sim_at_pred = plotter.interpolate_onto_grid(test_image_sim_data_at_pred)
        # plotter.plot_comparison(z_true, [z_pred, z_sim_near_pred, z_sim_at_pred], test_image_true_params, save_path=args.checkpoint_path + '/inversions', pred_params=[test_image_pred_params, test_image_sim_params_near_pred, test_image_pred_params], subtitles=('input', 'reconstruction', 'sim. near pred. params', 'sim. at pred. params'))
        plotter.plot_comparison(z_true, [z_sim_at_pred], test_image_true_params, save_path=args.checkpoint_path + '/inversions', pred_params=[test_image_pred_params], subtitles=('input', 'sim. at pred. params'))

    print('mean errors:')
    print(f'  absolute = {np.around(np.mean(abs_errors, axis=0), 2)}')
    print(f'  range-normalised = {np.around(np.mean(abs_errors / (np.float32(max_parameters) - min_parameters), axis=0), 4)}')
    print(f'  range-normalised relative = {np.around(np.mean(centered_rel_errors, axis=0), 4)}')
    print('median errors:')
    print(f'  absolute = {np.around(np.median(abs_errors, axis=0), 2)}')
    print(f'  range-normalised = {np.around(np.median(abs_errors / (np.float32(max_parameters) - min_parameters), axis=0), 4)}')
    print(f'  range-normalised relative = {np.around(np.median(centered_rel_errors, axis=0), 4)}')


if __name__ == '__main__':

    main()

