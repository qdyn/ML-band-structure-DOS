#ifndef MATRIX_H_
#define MATRIX_H_
#include "constants.h"
#include <complex>
typedef std::complex<double> Complex;

struct eigen {
	double value;
	double *vector;
	eigen(): vector(nullptr) {}
	~eigen() {delete[] vector;}
   
	bool operator<(eigen const &other) const {
		return value < other.value;
	}
};

struct Param {
	Param(): kvec(nullptr), hval(nullptr), eval(nullptr), muList(nullptr), dens(nullptr), DOS(nullptr) {}
	~Param() {delete[] kvec; delete[] hval; delete[] eval; delete[] muList; delete[] dens;
	delete[] DOS;}

	double Dt1;
	double mu;
	double Sk;
	int ksize;
	double* kvec;
	eigen* hval;
	eigen* eval;
	double* muList;
	double* dens;
	double* DOS;
};

struct BandParam {
	
	double dt;
	double g0;
	double g1;
	double g2;
	double g3;
	double g4;
    double g5;
	double Dt2;
	double v0;
	double v3;
	double v4;
};
	


Complex p(double kx, double ky);
Complex pc(double kx, double ky);
void getEigsMKL(double kx, double ky, const BandParam& Bp, double* evalues, Param& pm, int ind, int* isuppz);
void calcEigSystem(Param& pm, const BandParam& Bp);

#endif
