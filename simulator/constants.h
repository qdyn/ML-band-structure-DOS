#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#include <cmath>
#include <complex>
typedef std::complex<double> Complex;

const double sqrt3 = sqrt(3.0);
const double sqrt2 = sqrt(2.0);
const Complex I(0.0, 1.0);

const double a = 2.46;
const double dt = 35.5;
const double g0 = 3100.0;
const double g1 = 380.0;
const double g5 = 50;

const int ND = 1500;  // low-res: 500
const int sz = 3;
const int nuSym = 4;
const int gSym = 3;
const double kmax = 0.2;  // low-res: 0.15
const double xi = 1.0;


const int nCore = 4; //Specify the number of cores
const int muSize = 115001; // low-res: 9201
const int muSizer = 57501;  // low-res: 4601
const double dmu = 0.002;  // low-res: 0.025
const double mumax = 0.5 * (muSize - 1) * dmu;
const double T = 0.025;
const double Tm1 = 1.0 / T;
const int DSize = 151;  // low-res: 31
const double dDt = 0.5;  // low-res: 2.5
const double DtMax = 0.0;
const double muCut = 50;
#endif

