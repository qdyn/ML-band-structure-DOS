#include "constants.h"
#include "matrix.h"
#include "density.h"
#include <iostream>
#include <fstream>
#include <ctime>
#include "npy.hpp"

int main(int argc, char*argv[])
{
    std::clock_t c_start = std::clock();
	double* res = new double[4 * DSize * muSizer];
	if (argc != 5) {
		std::cout << "expected four arguments: g2, g3, g4, Dt2\n";
		return -1;
	}
	BandParam Bp;
	Bp.dt = dt;
	Bp.g0 = g0;
	Bp.g1 = g1;
	Bp.g2 = std::stod(argv[1]);
	Bp.g3 = std::stod(argv[2]);
	Bp.g4 = std::stod(argv[3]);
	Bp.g5 = g5;
	Bp.Dt2 = std::stod(argv[4]);
	Bp.v0 = 0.5 * sqrt3 * Bp.g0;
	Bp.v3 = 0.5 * sqrt3 * Bp.g3;
	Bp.v4 = 0.5 * sqrt3 * Bp.g4;
	std::cout << "running simulator at " << Bp.g2 << " " << Bp.g3 << " " << Bp.g4 << " " << Bp.Dt2 << std::endl;
	calcDens(res, Bp);
    std::vector<double> resv(res, res + 4 * DSize * muSizer);
    const long unsigned leshape [] = {DSize, muSizer, 4};
    char str[100];
    int i = 0;
    sprintf (str, "DOSNDTest%d.npy", i);
    npy::SaveArrayAsNumpy(str, false, 3, leshape, resv);

    std::clock_t c_end = std::clock();

    long double time_elapsed_ms = 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC;
    std::cout << "CPU time used: " << time_elapsed_ms << " ms\n";
	return 0;
}

