#include "matrix.h"
#include <thread>
#include <mkl_types.h>
#undef MKL_Complex16
#define MKL_Complex16 std::complex<double>
#include <mkl.h>


Complex p(double kx, double ky)
{
	return xi * kx + I * ky;
}

Complex pc(double kx, double ky)
{
	return xi * kx - I * ky;
}


void getEigsMKL(double kx, double ky, const BandParam& Bp, double* evalues, Param& pm, int ind, int* isuppz)
{
    Complex pl = p(kx, ky);
	Complex plc = std::conj(pl);
    Complex mat[4*sz*sz] {Bp.Dt2 - 0.5 * Bp.g2 - pm.mu, Bp.v0 * plc, pm.Dt1, 0.0, 0.0, 0.0,  // note pm.mu here is actually fixed to zero in calcDensDt!
			Bp.v0 * pl, Bp.Dt2 + Bp.dt - 0.5 * Bp.g5 - pm.mu, 0.0, 0.0, 0.0, pm.Dt1,
			pm.Dt1, 0.0, Bp.Dt2 + 0.5 * Bp.g2 - pm.mu, sqrt2 * Bp.v3 * pl, -sqrt2 * Bp.v4 * plc, Bp.v0 * plc,
			0.0, 0.0, sqrt2 * Bp.v3 * plc, -2.0 * Bp.Dt2 - pm.mu, Bp.v0 * pl, -sqrt2 * Bp.v4 * pl,
			0.0, 0.0, -sqrt2 * Bp.v4 * pl, Bp.v0 * plc, -2.0 * Bp.Dt2 + Bp.dt - pm.mu, sqrt2 * Bp.g1,
			0.0, pm.Dt1, Bp.v0 * pl, -sqrt2 * Bp.v4 * plc, sqrt2 * Bp.g1, Bp.Dt2 + Bp.dt + 0.5 * Bp.g5 - pm.mu};

	char jobz = 'N';
	char range = 'A';
	int size = 2 * sz;
	int m = 0;
	LAPACKE_zheevr(LAPACK_ROW_MAJOR, jobz, range, 'U', size, mat, size, 0.0, 0.0, 0, 0, 1E-6, &m, evalues, NULL, size, isuppz);
	for (int i = 0; i < sz; ++i) {
		pm.hval[ind * sz + i].value = evalues[i];
		pm.eval[ind * sz + i].value = evalues[sz + i];
	}
}


void calcEigSystem(Param& pm, const BandParam& Bp)
{
	double kx = 0.0;
	double ky = 0.0;
	double* evalues = new double[2 * sz];
	int* isuppz = new int[4 * sz];
	for (int kind = 0; kind < pm.ksize; ++kind) {
		kx = pm.kvec[2 * kind];
		ky = pm.kvec[2 * kind + 1];
		getEigsMKL(kx, ky, Bp, evalues, pm, kind, isuppz);
	}
	delete[] evalues;
	delete[] isuppz;
}
