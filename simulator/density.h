#ifndef DENSITY_H_
#define DENSITY_H_
#include "constants.h"
#include "matrix.h"

void calcDensDt(Param& pm, const BandParam& Bp);
double calcOccupSum(const Param& pm, double mu);
void calcDens(double* res, const BandParam& Bp);
void calcDensLoc(double* res, const double* DList, const BandParam& Bp, int DtStart, int DtEnd);
void kgrid(Param& pm, double kmax);

inline double nfe(double en, double mu, double Tm1)
{
	return 0.5 * (1.0 - std::tanh(0.5 * (en - mu) * Tm1));
}

inline double nfh(double en, double mu, double Tm1)
{
	return 0.5 * (1 - std::tanh(0.5 * (mu - en) * Tm1));
}

#endif
