#include "density.h"
#include <thread>
#include <stdio.h>
#include <cassert>

void calcDensDt(Param& pm, const BandParam& Bp)
{
	double densp = 0.0;
	double densn = 0.0;
	double mult = nuSym * gSym * pm.Sk / (pm.ksize * std::pow(2 * M_PI * 1E-2 * a, 2));
	pm.mu = 0.0;
	calcEigSystem(pm, Bp);

	pm.dens[0] = mult * calcOccupSum(pm, pm.muList[0]);
	densp = mult * calcOccupSum(pm, pm.muList[0] - dmu);
	densn = mult * calcOccupSum(pm, pm.muList[0] + dmu);
	pm.DOS[0] = (densn - densp) / (2 * dmu);
	pm.dens[1] = densn;	
	for (int i = 1; i < muSize - 1; ++i) {
		densp = pm.dens[i - 1];
		densn = mult * calcOccupSum(pm, pm.muList[i] + dmu);
		pm.DOS[i] = (densn - densp) / (2 * dmu);
		pm.dens[i + 1] = densn;
	}
	densp = pm.dens[muSize - 2];
	densn = mult * calcOccupSum(pm, pm.muList[muSize - 1] + dmu);
	pm.DOS[muSize - 1] = (densn - densp) / (2 * dmu);	
}

double calcOccupSum(const Param& pm, double mu)
{
	double nSum = 0.0;
	int Nl = sz * pm.ksize;
	double en = 0.0;
	double encut = 40 * T; 
	if (std::abs(mu) <= muCut) {
		for (int ie = 0; ie < Nl; ++ie) {
			en = pm.eval[ie].value;
			if (en - mu < -encut) {
				nSum += 1.0;
			}
			else if (en - mu < encut) {
				nSum += nfe(en, mu, Tm1);
			}
		}
		for (int ih = 0; ih < Nl; ++ih) {
			en = pm.hval[ih].value;
			if (mu - en < -encut) {
				nSum -= 1.0;
			}
			else if (mu - en < encut) {
				nSum -= nfh(en, mu, Tm1);
			}
		}
	}
	else if (mu < -muCut) {
		for (int ih = 0; ih < Nl; ++ih) {
			en = pm.hval[ih].value;
			if (mu - en < -encut) {
				nSum -= 1.0;
			}
			else if (mu - en < encut) {
				nSum -= nfh(en, mu, Tm1);
			}
		}
	}
	else {
		for (int ie = 0; ie < Nl; ++ie) {
			en = pm.eval[ie].value;
			if (en - mu < -encut) {
				nSum += 1.0;
			}
			else if (en - mu < encut) {
				nSum += nfe(en, mu, Tm1);
			}
		}
	}
	return nSum;
}


void calcDens(double* res, const BandParam& Bp)
{
	double* DList = new double[DSize];
	for (int i = 0; i < DSize; ++i) {
		DList[i] = -DtMax + i * dDt;
	}
	std::thread t[nCore - 1];
	for (int i = 0; i < nCore - 1; ++i) {
		t[i] = std::thread(calcDensLoc, res, DList, std::cref(Bp), i * DSize / nCore, (i + 1) * DSize / nCore);
	}
	calcDensLoc(res, DList, Bp, (nCore - 1) * DSize / nCore, DSize);

	for (int i = 0; i < nCore - 1; ++i) {
		t[i].join();
	}
	delete[] DList;
}

void calcDensLoc(double* res, const double* DList, const BandParam& Bp, int DtStart, int DtEnd)
{
	Param pm;
	kgrid(pm, kmax);
	pm.hval = new eigen[sz * pm.ksize];
	pm.eval = new eigen[sz * pm.ksize];
	pm.muList = new double[muSize];
	pm.dens = new double[muSize];
	pm.DOS = new double[muSize];
	for (int k = 0; k < muSize; ++k) {
		pm.muList[k] = -mumax + k * dmu;
	}
	for (int i = DtStart; i < DtEnd; ++i) {
		pm.Dt1 = DList[i];
		calcDensDt(pm, Bp);
		for (int k = 0; k < muSizer; ++k) {
			res[4 * i * muSizer + 4 * k] = DList[i];
			res[4 * i * muSizer + 4 * k + 1] = pm.muList[2*k];
			res[4 * i * muSizer + 4 * k + 2] = pm.dens[2*k];
			res[4 * i * muSizer + 4 * k + 3] = pm.DOS[2*k];
		}
	}
}

void kgrid(Param& pm, double kmax)
{
	double q1x = 1.0 * kmax;
	double q1y = 0.0 * kmax;
	double q2x = std::cos(2 * M_PI / 3.0) * kmax;
	double q2y = std::sin(M_PI / 3.0) * kmax;
	pm.Sk = std::abs(q1x * q2y - q1y * q2x);
	int ks = 0;
	pm.kvec = new double[2 * ND * ND];
	for (int i = 0; i < ND; ++i) {
		for (int j = 0; j < ND; ++j) {
			pm.kvec[2 * i * ND + 2 * j] = i * q1x / ND + j * q2x / ND;
			pm.kvec[2 * i * ND + 2 * j + 1] = i * q1y / ND + j * q2y / ND;
			++ks;
		}
	}
	pm.ksize = ks;
}

