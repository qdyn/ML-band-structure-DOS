
import os
import time
import subprocess
import numpy as np


def run(g2g3g4Dt2):

    src_path = os.path.dirname(os.path.realpath(__file__))
    start_time = time.time()
    result = subprocess.run(
        ['./jobABAML', *map(str, g2g3g4Dt2)],
        env={'LD_LIBRARY_PATH': os.environ['CONDA_PREFIX'] + '/lib'},
        cwd=src_path + '/simulator'
    )
    print(f'simulator took {time.time() - start_time:.3f}s')
    assert result.returncode == 0
    return np.load(f'{src_path}/simulator/DOSNDTest0.npy').astype(np.float32).reshape(-1, 4)

