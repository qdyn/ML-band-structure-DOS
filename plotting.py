
import os
import numpy as np
from dataclasses import dataclass


@dataclass
class Grid:
    x2: np.ndarray
    D1arr: np.ndarray
    xi: np.ndarray
    yi: np.ndarray


class Plotter:

    def __init__(self, args):

        x2 = np.arange(-2., 2., 0.005)  # sample points for n, along y-axis of plots
        D1arr = np.arange(args.Dt1_min, args.Dt1_max + 0.1, 2.5)  # sample points for Dt1, along x-axis of plots
        xi, yi = np.mgrid[slice(D1arr[0], D1arr[-1] + 0.001, D1arr[1] - D1arr[0]), slice(x2[0], x2[-1] + 0.001, x2[1] - x2[0])]
        self.grid = Grid(x2, D1arr, xi, yi)
        self.Dt1_min = args.Dt1_min
        self.Dt1_max = args.Dt1_max

    def interpolate_onto_grid(self, data):

        # Each row of data is [Dt1, mu, n, amplitude]

        from scipy.interpolate import splrep, splev

        d2len = len(self.grid.D1arr)
        x2len = len(self.grid.x2)
        z = np.zeros((d2len, x2len), dtype=float)
        # zd = np.zeros((d2len, x2len), dtype=float)
        for ii, D1val in np.ndenumerate(self.grid.D1arr):  # enumerate x-values (D1) of the plot
            a41 = data[np.abs(data[:, 0] - D1val) < 0.01, :]  # find the set of points whose col-0 is near D1 (in practice they're exactly aligned)
            u, indices = np.unique(a41[:, 2], return_index=True)
            x = a41[indices, 2]  # col-2 will become the y-coordinate; these are not regularly spaced, nor consistent between samples
            y = a41[indices, 3]  # col-3 will become the actual plotted values
            f = splrep(x, y, k=1, s=0.0)  # fit an interpolating spline
            z[ii, :] = splev(self.grid.x2, f)  # evaluate the spline at each point along the y-axis
            # zd[ii, :] = savgol_filter(z[si, ii, :], window_length=3, polyorder=2, deriv=1)

        return z

    def plot_single(self, z, params):

        import matplotlib.pyplot as plt

        plt.pcolormesh(self.grid.xi, self.grid.yi, 1 / (z + 1.0), vmin=0.0, vmax=1.)
        plt.colorbar()
        plt.xlim([self.Dt1_min, self.Dt1_max])
        plt.ylim([-2, 2])
        plt.xlabel(r'$\Delta_1$')
        plt.ylabel(r'$n, 10^{12} {\rm cm}^{-2}$')
        plt.title(r' $C_p\,(\gamma_2={},\gamma_3={},\gamma_4={},\Delta_2={})$'.format(*params))
        plt.show()

    def plot_comparison(self, z_true, z_pred, true_params, save_path=None, pred_params=None, subtitles=('true', 'predicted')):

        if not isinstance(z_pred, (list, tuple)):
            z_pred = [z_pred]
        if pred_params is not None and not isinstance(pred_params, (list, tuple)):
            pred_params = [pred_params]
        assert len(z_pred) == len(subtitles) - 1
        assert pred_params is None or len(z_pred) == len(pred_params)

        import matplotlib.pyplot as plt
        plt.clf()

        plt.subplot(len(z_pred), 3, 1)
        plt.pcolormesh(self.grid.xi, self.grid.yi, 1 / (z_true + 1.0), vmin=0.0, vmax=1.)
        plt.colorbar()
        plt.xlim([self.Dt1_min, self.Dt1_max])
        plt.ylim([-2, 2])
        plt.xlabel(r'$\Delta_1$')
        plt.ylabel(r'$n, 10^{12} {\rm cm}^{-2}$')
        plt.title(subtitles[0])

        for pred_index in range(len(z_pred)):

            plt.subplot(len(z_pred), 3, 3 * pred_index + 2)
            plt.pcolormesh(self.grid.xi, self.grid.yi, 1 / (z_pred[pred_index] + 1.0), vmin=0.0, vmax=1.)
            plt.colorbar()
            plt.xlim([self.Dt1_min, self.Dt1_max])
            plt.ylim([-2, 2])
            plt.xlabel(r'$\Delta_1$')
            plt.ylabel(r'$n, 10^{12} {\rm cm}^{-2}$')
            plt.title(subtitles[1 + pred_index])

            plt.subplot(len(z_pred), 3, 3 * pred_index + 3)
            plt.pcolormesh(self.grid.xi, self.grid.yi, np.abs(1 / (z_true + 1.0) - 1 / (z_pred[pred_index] + 1.0)))
            plt.colorbar()
            plt.xlim([self.Dt1_min, self.Dt1_max])
            plt.ylim([-2, 2])
            plt.xlabel(r'$\Delta_1$')
            plt.title('abs difference')

        if pred_params is None:
            plt.suptitle(r'$C_p\,(\gamma_2={:.2f},\gamma_3={:.1f},\gamma_4={:.1f},\Delta_2={:.2f})$'.format(*true_params))
        else:
            plt.subplots_adjust(top=0.82)
            plt.suptitle(r'true: $C_p\,(\gamma_2={:.2f},\gamma_3={:.1f},\gamma_4={:.1f},\Delta_2={:.2f})$' '\n' r'predicted: $C_p\,(\gamma_2={:.2f},\gamma_3={:.1f},\gamma_4={:.1f},\Delta_2={:.2f})$'.format(*true_params, *pred_params[0]))

        if save_path is not None:
            os.makedirs(save_path, exist_ok=True)
            plt.savefig('{}/g2={:.0f}_g3={:.0f}_g4={:.0f}_Delta2={:.0f}.png'.format(save_path, *true_params))
        else:
            plt.show()

    def plot_fig2(self, z_trues, z_preds, paramss, save_filename=None):

        assert len(z_trues) == len(z_preds) == len(paramss)
        assert self.Dt1_min == 0

        import matplotlib.pyplot as plt
        plt.clf()

        fig_size = plt.figaspect(1 / 4)
        fig, axs = plt.subplots(1, len(z_trues) * 3, sharex=True, sharey=True, figsize=fig_size)

        for plot_index in range(len(z_trues)):

            true_ax, pred_ax, err_ax = axs[plot_index * 3 : (plot_index + 1) * 3]
            true_ax.pcolormesh(self.grid.xi, self.grid.yi, 1 / (z_trues[plot_index] + 1.0), vmin=0.0, vmax=1., rasterized=True)
            true_ax.set_xlim(0, self.Dt1_max)
            true_ax.set_ylim(-2, 2)
            true_ax.set_xlabel(r'$\Delta_1$', fontdict={'fontsize' : 11})
            if plot_index == 0:
                true_ax.set_ylabel(r'$n, 10^{12} {\rm cm}^{-2}$', fontdict={'fontsize' : 11})
                true_ax.set_yticks([-2, -1, 0, 1, 2])
            else:
                true_ax.set_yticklabels([])
            true_ax.set_aspect(75 / 4 * 1.7)
            true_ax.text(1., 1.95, r'sim. $C_p$', verticalalignment='top')

            pred_ax.pcolormesh(self.grid.xi, self.grid.yi, 1 / (z_preds[plot_index] + 1.0), vmin=0.0, vmax=1., rasterized=True)
            pred_ax.set_xlim(0, self.Dt1_max)
            pred_ax.set_ylim(-2, 2)
            pred_ax.set_xlabel(r'$\Delta_1$', fontdict={'fontsize' : 11})
            pred_ax.set_title(r'$\gamma_2={:.2f},\gamma_3={:.1f},\gamma_4={:.1f},\Delta_2={:.2f}$'.format(*paramss[plot_index]), fontdict={'fontsize' : 12})
            pred_ax.set_yticklabels([])
            pred_ax.set_aspect(75 / 4 * 1.7)
            pred_ax.text(1., 1.95, r'DNN $C_p$', verticalalignment='top')

            err_ax.pcolormesh(self.grid.xi, self.grid.yi, np.abs(1 / (z_trues[plot_index] + 1.0) - 1 / (z_preds[plot_index] + 1.0)), rasterized=True)
            err_ax.set_xlim(self.Dt1_min, self.Dt1_max)
            err_ax.set_ylim(-2, 2)
            err_ax.set_xlabel(r'$\Delta_1$', fontdict={'fontsize' : 11})
            err_ax.set_yticklabels([])
            err_ax.set_aspect(75 / 4 * 1.7)
            err_ax.text(1., 1.95, r'abs. err', verticalalignment='top', color='white')

        # fig_size = plt.figaspect(1 / 5)
        # fig = plt.figure(constrained_layout=True, figsize=fig_size)
        # grid_spec = fig.add_gridspec(nrows=1, ncols=len(z_trues) * 2, wspace=0)
        #
        # for plot_index in range(len(z_trues)):
        #
        #     true_pred_ax = fig.add_subplot(grid_spec[0, plot_index * 2])
        #     true_pred_ax.pcolormesh(np.concatenate([-self.grid.xi, self.grid.xi], axis=1), np.tile(self.grid.yi, [1, 2]), 1 / (np.concatenate([z_trues[plot_index], z_preds[plot_index]], axis=1) + 1.0), vmin=0.0, vmax=1.)
        #     true_pred_ax.set_xlim(-self.Dt1_max, self.Dt1_max)
        #     true_pred_ax.set_ylim(-2, 2)
        #     true_pred_ax.set_xlabel(r'$\Delta_1$')
        #     true_pred_ax.set_ylabel(r'$n, 10^{12} {\rm cm}^{-2}$')
        #     true_pred_ax.set_title(r'$\gamma_2={},\gamma_3={},\gamma_4={},\Delta_2={}$'.format(*paramss[plot_index]), fontdict={'fontsize' : 10})
        #
        #     err_ax = fig.add_subplot(grid_spec[0, plot_index * 2 + 1])
        #     err_ax.pcolormesh(self.grid.xi, self.grid.yi, np.abs(1 / (z_trues[plot_index] + 1.0) - 1 / (z_preds[plot_index] + 1.0)))
        #     err_ax.set_xlim(self.Dt1_min, self.Dt1_max)
        #     err_ax.set_ylim(-2, 2)
        #     err_ax.set_xlabel(r'$\Delta_1$')
        #     err_ax.set_yticklabels([])

        if save_filename is not None:
            plt.savefig(save_filename)
        else:
            plt.show()
