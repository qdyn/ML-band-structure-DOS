
import cv2
import math
import argparse
import numpy as np
import scipy.io
import scipy.optimize
import scipy.interpolate
import tensorflow as tf
import matplotlib.pyplot as plt
import seaborn as sns
from tqdm import tqdm

import data
import regressor
import simulator
import plotting


def load_d9_data(d9_mat_path):

    M = scipy.io.loadmat(d9_mat_path)
    d9 = M['d9']

    # For the column structure, see start of leocap4a_d9_plotnotes.m

    x_indices = d9[:, 0]
    y_indices = d9[:, 1]
    width = int(np.max(x_indices)) + 1
    height = int(np.max(y_indices)) + 1
    assert len(d9) == width * height

    C_p, p, n = map(lambda x: x.reshape([height, width]), (d9[:, 4], d9[:, 6], d9[:, 7]))

    n *= 0.9418  # see Combined_TLG_paper_plots2.m:22
    D1 = p * 3.25 / 40 * 100.  # see Combined_TLG_paper_plots2.m:26
    # note the above scaling is not critical, since the optimisation fits a scale anyway

    return D1, n, C_p


def plot_d9_data(args, D1, n, C_p, limit_range, flip=False):

    if limit_range:
        C_p  = C_p.copy()
        C_p[np.logical_or(D1 < -args.Dt1_max, D1 > args.Dt1_max)] = np.nan
        C_p[np.logical_or(n < args.n_min, n > args.n_max)] = np.nan
    plt.pcolormesh(D1, n, C_p[::-1 if flip else 1], vmin=1.255, vmax=1.290, cmap='magma')
    plt.title('experimental')
    plt.show()


def get_d9_parameters(scaled=True):

    # These are the values on p2 of Zibrov PRL '18
    g0 = 3.1  # fixed
    g1 = 0.38  # fixed
    g2 = -0.021  # -0.016 -- -0.026
    g3 = 0.29  # no error bar
    g4 = 0.141  # 0.101 -- 0.181
    g5 = 0.050  # fixed
    Dt2 = 0.0035  # 0.0033 -- 0.0037
    dt = 0.0355  # fixed

    if scaled:
        # These bring the values into the range of parameters for the simulated data
        return g2 * 1.e3, g3 * -1.e3, g4 * 1.e3, Dt2 * 1.e3
    else:
        return g2, g3, g4, Dt2


def plot_simulated_data_near_parameters(args, D1, n, parameters, xlim, ylim, title_suffix):

    g2_nearest = data.g2_range[np.argmin(np.abs(data.g2_range - parameters[0]))]
    g3_nearest = data.g3_range[np.argmin(np.abs(data.g3_range - parameters[1]))]
    g4_nearest = data.g4_range[np.argmin(np.abs(data.g4_range - parameters[2]))]
    Dt2_nearest = data.Dt2_range[np.argmin(np.abs(data.Dt2_range - parameters[3]))]

    simulated_values = data.load_raw(args.data_path, g2_nearest, g3_nearest, g4_nearest, Dt2_nearest)  # point, D1/mu/n/nu
    simulated_D1, simulated_mu, simulated_n, simulated_nu = simulated_values.T

    eta = 5.e3
    mu_offset = simulated_mu[simulated_D1 == 0.][np.argmin(np.abs(simulated_n[simulated_D1 == 0.]))]

    nu_interpolator = scipy.interpolate.LinearNDInterpolator(np.stack([simulated_D1, simulated_n + (simulated_mu - mu_offset) / eta], axis=1), simulated_nu)
    interpolated_nu = nu_interpolator(np.abs(D1), n)

    plt.pcolormesh(D1, n, 1. / (1. + interpolated_nu), vmin=0.8, vmax=1.1, cmap='magma')
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.title(f'simulated nearest {title_suffix} (g2 = {g2_nearest:.1f}, g3 = {g3_nearest:.0f}, g4 = {g4_nearest:.0f}, Dt2 = {Dt2_nearest:.1f})')
    plt.show()


def plot_simulated_data_near_experimental_parameters(args, experimental_D1, experimental_n):

    return plot_simulated_data_near_parameters(
        args,
        experimental_D1, experimental_n,
        get_d9_parameters(),
        [experimental_D1.min(), experimental_D1.max()],
        [experimental_n.min(), experimental_n.max()],
        "exp't"
    )


def simulate_and_plot_on_grid(parameters, D1, n, xlim, ylim, title_suffix):

    simulated_values = simulator.run(parameters)
    simulated_D1, simulated_mu, simulated_n, simulated_nu = simulated_values.T

    eta = 5.e3
    mu_offset = simulated_mu[simulated_D1 == 0.][np.argmin(np.abs(simulated_n[simulated_D1 == 0.]))]

    nu_interpolator = scipy.interpolate.LinearNDInterpolator(np.stack([simulated_D1, simulated_n + (simulated_mu - mu_offset) / eta], axis=1), simulated_nu)
    interpolated_nu = nu_interpolator(np.abs(D1), n)

    plt.pcolormesh(D1, n, 1. / (1. + interpolated_nu), vmin=0.8, vmax=1.1, cmap='magma')
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.title(f'simulated at {title_suffix} (g2 = {parameters[0]:.1f}, g3 = {parameters[1]:.0f}, g4 = {parameters[2]:.0f}, Dt2 = {parameters[3]:.1f})')
    plt.show()


def predict_densities_for_parameters(model, parameters, Dt1, n, eta):

    # The input parameters should be float32 ndarrays/tensors
    # parameters :: *, g2/g3/g4/Dt2 where * is the axes of both Dt1 and n

    Dt1s_and_ns_flat = tf.reshape(tf.stack([tf.abs(Dt1), n], axis=-1), [-1, 2])
    inputs = tf.concat([
        tf.broadcast_to(parameters, [Dt1s_and_ns_flat.shape[0], parameters.shape[0]]),
        Dt1s_and_ns_flat,
        tf.reshape(eta, [-1, 1])
    ], axis=1)
    predicted_densities = tf.squeeze(model(inputs), axis=1)
    predicted_densities = tf.reshape(predicted_densities, Dt1.shape)

    return predicted_densities


def plot_regressed_data_for_experimental_parameters(args, model, Dt1_exp, n):

    g234Dt2 = np.float32(get_d9_parameters())

    alpha = 1.
    beta = 1.
    gamma = 1.
    eta = args.eta_max
    Dt1_sim = (Dt1_exp * alpha).astype(np.float32)

    predicted_densities = predict_densities_for_parameters(model, g234Dt2, Dt1_sim, n, tf.broadcast_to(eta, n.shape)).numpy()

    predicted_C_p = beta / (gamma + predicted_densities)
    predicted_C_p[np.logical_or(Dt1_sim < -model.Dt1_range[1], Dt1_sim > model.Dt1_range[1])] = np.nan
    predicted_C_p[np.logical_or(n < model.n_range[0], n > model.n_range[1])] = np.nan

    plt.pcolormesh(Dt1_exp, n, predicted_C_p, vmin=0.8, vmax=1.1, cmap='magma')
    plt.title('regressed at experimental parameters')
    plt.show()


class SmoothMonotonicDeformation:

    Dt1_max = 75.
    n_min = -1.45
    n_max = 1.45
    Dt1_steps = 2  # minimum of two, corresponding to Dt1 = 0 and Dt1 = max
    n_steps_per_side = 2  # minimum of one, corresponding to n = max (there's an implicit point at n=0)

    @classmethod
    def from_parameters(cls, parameters):
        return SmoothMonotonicDeformation(*tf.split(tf.reshape(parameters, [2 * cls.Dt1_steps, cls.n_steps_per_side]), num_or_size_splits=2, axis=0))

    @classmethod
    def get_zero_parameters(cls):
        return np.zeros([cls.Dt1_steps * cls.n_steps_per_side * 2])

    @classmethod
    def print_parameters(cls, parameters):
        relative_displacements_above, relative_displacements_below = tf.split(tf.reshape(tf.exp(parameters), [2 * cls.Dt1_steps, cls.n_steps_per_side]), num_or_size_splits=2, axis=0)
        print('  deformation displacements (n > 0): ', relative_displacements_above.numpy())
        print('  deformation displacements (n < 0): ', relative_displacements_below.numpy())

    def __init__(self, log_relative_displacements_above, log_relative_displacements_below):
        self._log_relative_displacements_above = log_relative_displacements_above  # indexed by Dt1-step (horz.), n-step (vert.)
        self._log_relative_displacements_below = log_relative_displacements_below

    def __call__(self, Dt1, n):

        # Dt1 and n must be of the same (arbitrary) shape; points are processed independently

        # For each point, choose the n>0 or n<0 parameters appropriately
        log_relative_displacements = tf.where(tf.greater_equal(n, 0.)[..., None, None], self._log_relative_displacements_above, self._log_relative_displacements_below)  # point*, Dt1-step, n-step

        # Find the Dt1-segment that each point falls in; note the lower index; lerp the parameter vectors
        abs_Dt1 = tf.abs(Dt1)
        Dt1_step_size = self.Dt1_max / (self.Dt1_steps - 1)
        Dt1_step_below = tf.cast(abs_Dt1 / Dt1_step_size, tf.int64)
        Dt1_frac = (abs_Dt1 % Dt1_step_size) / Dt1_step_size
        log_relative_displacements_at_Dt1 = tf.gather(log_relative_displacements, Dt1_step_below, batch_dims=Dt1.shape.ndims) * (1. - Dt1_frac)[..., None] + tf.gather(log_relative_displacements, Dt1_step_below + 1, batch_dims=Dt1.shape.ndims) * Dt1_frac[..., None]  # point*, n-step

        # Find the n-segment that each point falls in; note the lower index
        abs_n = tf.abs(n)
        assert self.n_min < 0 < self.n_max
        n_max_or_min = tf.where(tf.greater_equal(n, 0.), self.n_max, abs(self.n_min))
        n_step_size = n_max_or_min / self.n_steps_per_side
        n_step_below = tf.cast(abs_n / n_step_size, tf.int64)  # note 'below' = 'near to zero' here!
        n_frac = (abs_n % n_step_size) / n_step_size

        # For each point's Dt1, find the n's that the vector of n-anchors maps to; lerp these based on the points' n's
        n_vectors_at_Dt1 = tf.concat([
            tf.zeros(n.shape.as_list() + [1]),
            tf.cumsum(n_step_size[..., None] * tf.exp(log_relative_displacements_at_Dt1), axis=-1)
        ], axis=-1)  # point*, n-step
        n_transformed_unsigned = tf.gather(n_vectors_at_Dt1, n_step_below, batch_dims=Dt1.shape.ndims) * (1. - n_frac) + tf.gather(n_vectors_at_Dt1, n_step_below + 1, batch_dims=Dt1.shape.ndims) * n_frac

        return Dt1, n_transformed_unsigned * tf.sign(n), None


class AlphaEtaDeformation:

    @classmethod
    def from_parameters(cls, parameters):
        return AlphaEtaDeformation(*tf.split(parameters, num_or_size_splits=2))

    @classmethod
    def get_zero_parameters(cls):
        return np.zeros([6])  # electron-/hole-side * alpha_1/alpha_3/eta

    @classmethod
    def print_parameters(cls, parameters):
        electron_side_parameters, hole_side_parameters = tf.split(parameters, num_or_size_splits=2)
        print(f'  deformation parameters (n > 0): alpha_1 = {np.exp(electron_side_parameters[0]):.2f}, alpha_3 * 10^4 = {electron_side_parameters[1]:.3f}, eta = {np.exp(electron_side_parameters[2]):.1f}')
        print(f'  deformation parameters (n < 0): alpha_1 = {np.exp(hole_side_parameters[0]):.2f}, alpha_3 * 10^4 = {hole_side_parameters[1]:.3f}, eta = {np.exp(hole_side_parameters[2]):.1f}')

    def __init__(self, electron_side_parameters, hole_side_parameters):
        self._electron_side_parameters = electron_side_parameters
        self._hole_side_parameters = hole_side_parameters

    def __call__(self, Dt1_exp, n):

        transform_parameters = tf.where(
            tf.greater_equal(n, 0)[None],
            self._electron_side_parameters[:, None, None],
            self._hole_side_parameters[:, None, None]
        )
        # transform_parameters specifies for each point: [log(alpha_1), alpha_3 * 1.e4, log(eta)]

        Dt1_sim = Dt1_exp * tf.exp(transform_parameters[0]) + Dt1_exp**3 * (transform_parameters[1] * 1.e-4)

        return Dt1_sim, n, transform_parameters[2]


class PiecewiseLinearMonotonicFunction:

    exp_range = 1.255, 1.290
    steps = 6

    @classmethod
    def from_parameters(cls, parameters):
        return PiecewiseLinearMonotonicFunction(*tf.split(parameters, [2, 2, cls.steps, cls.steps]))

    @classmethod
    def get_num_parameters(cls):
        return 4 + cls.steps * 2

    @classmethod
    def get_suggested_bounds(cls):
        return (
            [0., 0.2, 1., 1.2] + [-2.] * cls.steps * 2,  # sim_min, sim_max, exp_min, exp_max, step-logits
            [0.1, 0.7, 1.5, 1.7] + [2.] * cls.steps * 2,
        )

    @classmethod
    def print_parameters(cls, parameters):
        pass

    def __init__(self, sim_range, exp_range, electron_side_logits, hole_side_logits):
        # self._sim_range = sim_range
        # self._exp_range = exp_range
        self._electron_side_logits = electron_side_logits
        self._hole_side_logits = hole_side_logits

    def __call__(self, Cp_sim, n):

        sim_range = tf.reduce_min(Cp_sim), tf.reduce_max(Cp_sim)

        normalised_step_exp = tf.where(
            tf.greater_equal(n, 0)[:, :, None],
            tf.cumsum(tf.nn.softmax(self._electron_side_logits), exclusive=True),
            tf.cumsum(tf.nn.softmax(self._hole_side_logits), exclusive=True)
        )  # step, *
        normalised_step_exp = tf.concat([normalised_step_exp, tf.ones_like(normalised_step_exp[:, :, :1])], axis=-1)

        steps_exp = self.exp_range[0] + (self.exp_range[1] - self.exp_range[0]) * normalised_step_exp  # Dt1-index, n-index, step

        step_size_sim = (sim_range[1] - sim_range[0]) / self.steps
        step_index = tf.cast((Cp_sim - sim_range[0]) / step_size_sim, tf.int64)
        step_index = tf.clip_by_value(step_index, 0, self.steps - 1)
        prev_step_sim = sim_range[0] + tf.cast(step_index, tf.float32) * step_size_sim
        step_frac = (Cp_sim - prev_step_sim) / step_size_sim
        prev_step_exp = tf.gather(steps_exp, step_index, batch_dims=2)
        next_step_exp = tf.gather(steps_exp, step_index + 1, batch_dims=2)

        Cp_exp = (1. - step_frac) * prev_step_exp + step_frac * next_step_exp

        return Cp_exp


def get_transformed_Cp_predictions_and_validities(parameters, model, deformer_cls, Dt1_exp, n):

    # All parameters should be float32 tensors
    # parameters has four blocks: nu-to-Cp, planar-rigid, planar-deformation, Cp-to-Cp, simulator
    # Size and interpretation of deformer-parameters is defined by deformer_cls
    # nu-to-Cp-parameters is electron-/hole-side * log(beta) / log(gamma)
    # planar-rigid is x-/y-shift, rotation; this is applied after the planar deformation
    # simulator-parameters is [g2, g3, g4, Dt2]

    (
        electron_side_nu_to_Cp_parameters, hole_side_nu_to_Cp_parameters,
        rigid_shift, rigid_rotation,
        deformation_parameters,
        CpCp_parameters,
        simulator_parameters
    ) = tf.split(parameters, [2, 2, 2, 1, len(deformer_cls.get_zero_parameters()), PiecewiseLinearMonotonicFunction.get_num_parameters(), 4])

    deformer = deformer_cls.from_parameters(deformation_parameters)
    Dt1_sim, n, log_eta = deformer(Dt1_exp, n)
    if log_eta is None:
        log_eta = tf.ones_like(Dt1_sim) * tf.math.log(model.eta_range[1])

    rigid_cos, rigid_sin = tf.cos(rigid_rotation), tf.sin(rigid_rotation)
    Dt1_sim, n = (
        # Note the factor of 1e2 here is to improve the 'conditioning' -- so we're rotating a roughly-square plot not a very elogated one
        (rigid_cos * Dt1_sim / 1.e2 - rigid_sin * n) * 1.e2 + rigid_shift[0],
        rigid_sin * Dt1_sim / 1.e2 + rigid_cos * n + rigid_shift[1]
    )

    predicted_nu = predict_densities_for_parameters(model, simulator_parameters, Dt1_sim, n, tf.exp(log_eta))

    inputs_valid = tf.logical_and(
        tf.logical_and(tf.less_equal(-model.Dt1_range[1], Dt1_sim), tf.less_equal(Dt1_sim, model.Dt1_range[1])),
        tf.logical_and(tf.less_equal(model.n_range[0], n), tf.less_equal(n, model.n_range[1])),
    )

    nu_to_Cp_parameters = tf.where(
        tf.greater_equal(n, 0)[None],
        electron_side_nu_to_Cp_parameters[:, None, None],
        hole_side_nu_to_Cp_parameters[:, None, None]
    )
    Cp = tf.exp(nu_to_Cp_parameters[0]) / (tf.exp(nu_to_Cp_parameters[1]) + predicted_nu)

    Cp_to_Cp = PiecewiseLinearMonotonicFunction.from_parameters(CpCp_parameters)
    Cp_adjusted = Cp_to_Cp(Cp, n)

    return Cp_adjusted, inputs_valid


def preprocess_experimental(Cp):

    Cp = cv2.GaussianBlur(Cp, (15, 15), 3.)
    return Cp


def gaussian_pyramid(pixels, levels=None, no_channels=False):

    # pixels is indexed by *, y, x, channel; dimensions * and channel are assumed to have static shape
    # If levels is None, then it is set automatically such that the smallest scale is 1x1
    # If no_channels is True, then neither x nor the result includes the trailing channel dimension

    if no_channels:
        pixels = pixels[..., np.newaxis]

    if levels is None:
        size = max(int(pixels.get_shape()[-2]), int(pixels.get_shape()[-3]))
        levels = int(math.ceil(math.log(size) / math.log(2))) + 1

    assert levels > 0  # includes the original scale

    kernel_sigma = 1.
    kernel_size = 3
    kernel_1d = cv2.getGaussianKernel(kernel_size, kernel_sigma)
    kernel = tf.constant(np.tile((kernel_1d * kernel_1d.T)[:, :, np.newaxis, np.newaxis], [1, 1, int(pixels.get_shape()[-1]), 1]), dtype=tf.float32)

    pyramid = [tf.reshape(pixels, [-1] + pixels.get_shape()[-3:].as_list())]
    for level in range(levels - 1):
        downsampled = tf.nn.depthwise_conv2d(pyramid[-1], kernel, [1, 2, 2, 1], 'SAME')
        pyramid.append(downsampled)

    result_with_channels = [tf.reshape(level, pixels.get_shape()[:-3].concatenate(level.get_shape()[-3:])) for level in pyramid]

    if no_channels:
        return [result_level[..., 0] for result_level in result_with_channels]
    else:
        return result_with_channels


def get_multiscale_grad_loss(x, y, validities, loss_mode, levels=5, verbose=None):

    downsample_factor = 1
    x_downsampled = tf.nn.avg_pool2d(x[None, :, :, None], downsample_factor, [1, downsample_factor, downsample_factor, 1], padding='VALID')
    y_downsampled = tf.nn.avg_pool2d(y[None, :, :, None], downsample_factor, [1, downsample_factor, downsample_factor, 1], padding='VALID')
    validities_downsampled = -tf.nn.max_pool2d(-tf.cast(validities, tf.float32)[None, :, :, None], downsample_factor, [1, downsample_factor, downsample_factor, 1], padding='VALID')

    derivative_kernel_1d = tf.constant([-0.5, 0., 0.5], dtype=tf.float32)
    derivative_kernel_x = tf.tile(derivative_kernel_1d[None, :, None, None], [3, 1, 1, 1])
    derivative_kernel_y = tf.tile(derivative_kernel_1d[:, None, None, None], [1, 3, 1, 1])
    derivatives_kernel = tf.concat([derivative_kernel_x, derivative_kernel_y], axis=-1)

    grad_x = tf.nn.depthwise_conv2d(x_downsampled, derivatives_kernel, strides=[1, 1, 1, 1], padding='VALID')[0, 1:-1, 1:-1]
    grad_y = tf.nn.depthwise_conv2d(y_downsampled, derivatives_kernel, strides=[1, 1, 1, 1], padding='VALID')[0, 1:-1, 1:-1]
    grad_validities = -tf.nn.max_pool2d(-validities_downsampled, 3, strides=[1, 1, 1, 1], padding='VALID')[0, 1:-1, 1:-1, 0]

    x_pyramid = gaussian_pyramid(x, levels, no_channels=True)
    y_pyramid = gaussian_pyramid(y, levels, no_channels=True)
    validities_pyramid = gaussian_pyramid(tf.cast(validities, tf.float32), levels, no_channels=True)
    grad_x_pyramid = gaussian_pyramid(grad_x, levels)
    grad_y_pyramid = gaussian_pyramid(grad_y, levels)
    grad_validities_pyramid = gaussian_pyramid(grad_validities, levels, no_channels=True)

    total_loss = 0.
    for level, (x_level, y_level, grad_x_level, grad_y_level, validities_level, grad_validities_level) in list(enumerate(zip(x_pyramid, y_pyramid, grad_x_pyramid, grad_y_pyramid, validities_pyramid, grad_validities_pyramid))):
        validities_level = tf.greater_equal(validities_level, 0.9999)
        grad_validities_level = tf.greater_equal(grad_validities_level, 0.9999)

        if loss_mode == 'val-mae':
            # MAE (values)
            errors = tf.abs(x_level - y_level)
            total_loss += tf.reduce_mean(tf.boolean_mask(errors, validities_level))

        elif loss_mode == 'val-msqrte':
            # mean sqrt abs error (values)
            errors = tf.where(tf.equal(x_level, y_level), 0., tf.sqrt(tf.abs(x_level - y_level)))
            total_loss += tf.reduce_mean(tf.boolean_mask(errors, validities_level))

        elif loss_mode == 'val-zncc':
            # ZNCC (values)
            x_mean, x_var = tf.nn.moments(x_level, axes=[0, 1], keepdims=True)
            y_mean, y_var = tf.nn.moments(y_level, axes=[0, 1], keepdims=True)
            x_normalised = (x_level - x_mean) / tf.sqrt(tf.maximum(0., x_var))
            y_normalised = (y_level - y_mean) / tf.sqrt(tf.maximum(0., y_var))
            total_loss -= tf.reduce_mean(x_normalised * y_normalised)

        elif loss_mode == 'grad-mae':
            # MAE (grads)
            errors = tf.reduce_sum(tf.abs(grad_x_level - grad_y_level), axis=-1)
            total_loss += tf.reduce_mean(tf.boolean_mask(errors, grad_validities_level))

        elif loss_mode == 'grad-znae':
            # ZNAE (grads)
            x_mean, x_var = tf.nn.moments(grad_x_level, axes=[0, 1], keepdims=True)
            y_mean, y_var = tf.nn.moments(grad_y_level, axes=[0, 1], keepdims=True)
            x_normalised = (grad_x_level - x_mean) / tf.sqrt(tf.maximum(0., x_var))
            y_normalised = (grad_y_level - y_mean) / tf.sqrt(tf.maximum(0., y_var))
            errors = tf.reduce_sum(tf.abs(x_normalised - y_normalised), axis=-1)
            total_loss += tf.reduce_mean(tf.boolean_mask(errors, grad_validities_level))

        elif loss_mode == 'grad-zncc':
            # ZNCC (grads)
            x_mean, x_var = tf.nn.moments(grad_x_level, axes=[0, 1], keepdims=True)
            y_mean, y_var = tf.nn.moments(grad_y_level, axes=[0, 1], keepdims=True)
            x_normalised = (grad_x_level - x_mean) / tf.sqrt(tf.maximum(0., x_var))
            y_normalised = (grad_y_level - y_mean) / tf.sqrt(tf.maximum(0., y_var))
            total_loss -= tf.reduce_mean(x_normalised * y_normalised)

        else:
            assert False

        if verbose is not None:
            grad_x_normed = (grad_x_level - np.quantile(grad_x_level.numpy(), 0.05)) / (np.quantile(grad_x_level.numpy(), 0.95) - np.quantile(grad_x_level.numpy(), 0.05))  # x, y, channel
            grad_y_normed = (grad_y_level - np.quantile(grad_y_level.numpy(), 0.05)) / (np.quantile(grad_y_level.numpy(), 0.95) - np.quantile(grad_y_level.numpy(), 0.05))
            grads_rgb = tf.concat([
                tf.reshape(tf.transpose(grad_x_normed, [2, 1, 0]), [-1, grad_x_normed.shape[0]]),
                tf.reshape(tf.transpose(grad_y_normed, [2, 1, 0]), [-1, grad_y_normed.shape[0]]),
            ], axis=1)[::-1]
            tf.io.write_file(f'./output/losses/{verbose}_{level}.png', tf.image.encode_png(tf.cast(tf.clip_by_value(grads_rgb[..., None], 0., 1.) * 255., tf.uint8)))

    return total_loss / len(grad_x_pyramid)


def get_loss_and_grad(parameters, model, deformer_cls, Dt1, n, experimental_Cp, loss_mode, verbose=None):

    parameters = tf.convert_to_tensor(parameters.astype(np.float32))

    with tf.GradientTape() as tape:
        tape.watch(parameters)
        predicted_Cp, validities = get_transformed_Cp_predictions_and_validities(parameters, model, deformer_cls, Dt1, n)
        # recon_loss = tf.reduce_mean(tf.boolean_mask(tf.abs(predicted_Cp - experimental_Cp), validities))
        # recon_loss = get_grad_pyramid_loss(predicted_Cp, experimental_Cp, validities, verbose=verbose)
        recon_loss = get_multiscale_grad_loss(predicted_Cp, experimental_Cp, validities, loss_mode, verbose=verbose)

    grad = tape.gradient(recon_loss, parameters)
    return recon_loss.numpy().astype(np.float64), grad.numpy().astype(np.float64)


def get_jacobian_and_hessian(all_parameters, model, deformer_cls, Dt1, n, experimental_Cp, loss_mode, verbose=None):

    physical_parameters = all_parameters[-4:]
    other_parameters = all_parameters[:-4]

    with tf.GradientTape(persistent=True) as outer_tape:
        outer_tape.watch(physical_parameters)
        with tf.GradientTape(persistent=True) as inner_tape:
            inner_tape.watch(physical_parameters)
            parameters = tf.concat([other_parameters, physical_parameters], axis=0)
            predicted_Cp, validities = get_transformed_Cp_predictions_and_validities(parameters, model, deformer_cls, Dt1, n)
            recon_loss = get_multiscale_grad_loss(predicted_Cp, experimental_Cp, validities, loss_mode, verbose=verbose)
        J = inner_tape.jacobian(recon_loss, physical_parameters, experimental_use_pfor=False)
    H = outer_tape.jacobian(J, physical_parameters, experimental_use_pfor=False)

    return J.numpy(), H.numpy()


def plot_sensitivities(best_parameters, g234Dt2_range, model, deformer_cls, Dt1, n, experimental_Cp, loss_mode, plot_for_parameters):

    param_names = ['g2', 'g3', 'g4', 'Dt2']
    lower_g234Dt2, upper_g234Dt2 = g234Dt2_range
    line_search_step = (upper_g234Dt2 - lower_g234Dt2) / 50.

    best_loss = get_loss_and_grad(best_parameters, model, deformer_cls, Dt1, n, experimental_Cp, loss_mode)[0]
    plot_for_parameters(best_parameters, 'best parameters', use_simulator=False)

    loss_threshold = 0.01 * abs(best_loss) + best_loss
    plot_steps = 100
    binary_search_steps = 20

    def params_substituting(param_index, value):
        return np.concatenate([best_parameters[:-4 + param_index], [value], best_parameters[len(best_parameters) - 4 + param_index + 1:]])
    def evaluate_substituting(param_index, value):
        return get_loss_and_grad(params_substituting(param_index, value), model, deformer_cls, Dt1, n, experimental_Cp, loss_mode)[0]

    param_to_plot_values = {}
    for param_index, param_name in enumerate(param_names):

        def binary_search(left, right):
            left_loss = evaluate_substituting(param_index, left)
            for _ in range(binary_search_steps):
                midpoint = (left + right) / 2
                loss = evaluate_substituting(param_index, midpoint)
                if (loss - loss_threshold > 0) == (left_loss - loss_threshold > 0):
                    left = midpoint
                    left_loss = loss
                else:
                    right = midpoint
            return (left + right) / 2

        def line_search(direction):
            param_value = best_parameters[-4 + param_index]
            while True:
                loss = evaluate_substituting(param_index, param_value)
                if loss > loss_threshold:
                    return binary_search(param_value, param_value - direction * line_search_step[param_index])
                param_value += direction * line_search_step[param_index]
        param_upper = line_search(1.)
        param_lower = line_search(-1.)

        rel_diff_upper = (param_upper - best_parameters[-4:]) / np.abs(best_parameters[-4:])
        rel_diff_lower = (param_lower - best_parameters[-4:]) / np.abs(best_parameters[-4:])

        print(f'{param_name}: lower = {param_lower:.2f} ({rel_diff_lower[param_index] * 100:.2f}%), best = {best_parameters[-4 + param_index]:.2f}, upper = {param_upper:.2f} ({rel_diff_upper[param_index] * 100:.2f}%)')
        plot_for_parameters(params_substituting(param_index, param_lower), f'{param_name} = {param_lower:.2f} (lower)', use_simulator=False)
        plot_for_parameters(params_substituting(param_index, param_upper), f'{param_name} = {param_upper:.2f} (upper)', use_simulator=False)

        plot_values = []
        for param_value in np.linspace(param_lower, param_upper, plot_steps):
            plot_values.append((param_value, evaluate_substituting(param_index, param_value)))
        param_to_plot_values[param_name] = np.asarray(plot_values)

    fig, axes = plt.subplots(1, len(param_names), sharey=True, figsize=plt.figaspect(1 / 3))
    for param_index, param_name in enumerate(param_names):
        axes[param_index].plot(param_to_plot_values[param_name][:, 0], param_to_plot_values[param_name][:, 1])
        # axes[param_index].axvline(x=best_parameters[-4 + param_index])
        axes[param_index].set_title(param_name)
    plt.show()


class SimulatorPseudoModel:

    def __init__(self, model):
        self._model = model
        self.Dt1_range = model.Dt1_range
        self.n_range = model.n_range
        self.eta_range = model.eta_range

    def __call__(self, inputs):
        # inputs :: point-index, g234Dt2/Dt1/n_exp/eta
        g234Dt2 = inputs[0, :4].numpy()
        simulated_values = simulator.run(g234Dt2)
        simulated_D1, simulated_mu, simulated_n, simulated_nu = simulated_values.T
        etas = inputs[:, 6].numpy()
        unique_etas = np.unique(etas)
        assert len(unique_etas) == 2  # should contain one value for electron side and one for hole side
        mu_offset = simulated_mu[simulated_D1 == 0.][np.argmin(np.abs(simulated_n[simulated_D1 == 0.]))]
        D1 = inputs[:, 4].numpy()
        n = inputs[:, 5].numpy()
        interpolated_nu = np.zeros([inputs.shape[0]])
        for eta in unique_etas:
            nu_interpolator = scipy.interpolate.LinearNDInterpolator(np.stack([simulated_D1, simulated_n + (simulated_mu - mu_offset) / eta], axis=1), simulated_nu)
            interpolated_nu_for_this_eta = nu_interpolator(np.abs(D1), n)
            interpolated_nu[etas == eta] = interpolated_nu_for_this_eta[etas == eta]
        nn_nu = self._model(inputs)  # clip to same range as NN, so PMLD behaves the same
        nn_range = tf.reduce_min(nn_nu).numpy(), tf.reduce_max(nn_nu).numpy()
        interpolated_nu = interpolated_nu.clip(*nn_range)
        return tf.constant(interpolated_nu[:, None].astype(np.float32))


def fit_to_Cp(model, joint_Dt1, joint_n, cropped_experimental_Cp, true_g234Dt2, g234Dt2_range, restarts, plot_x_range, plot_y_range):

    lower_g234Dt2, upper_g234Dt2 = g234Dt2_range

    lower_beta_gamma = -0.1, -2.
    upper_beta_gamma = 0.1, 3.

    lower_rigid = [-2.5, -0.1, -0.05]
    upper_rigid = [2.5, 0.1, 0.05]

    lower_CpCp, upper_CpCp = PiecewiseLinearMonotonicFunction.get_suggested_bounds()

    deformer_cls = AlphaEtaDeformation
    lower_deformation = np.float32([-1., 0., 4] * 2)
    upper_deformation = np.float32([0.1, 1., 8.5] * 2)

    min_parameters = np.concatenate([lower_beta_gamma * 2, lower_rigid, lower_deformation, lower_CpCp, lower_g234Dt2])
    max_parameters = np.concatenate([upper_beta_gamma * 2, upper_rigid, upper_deformation, upper_CpCp, upper_g234Dt2])

    parameters = tf.Variable(initial_value=(min_parameters + max_parameters) / 2, trainable=True, dtype=tf.float32)

    def plot_for_parameters(parameters, title, use_simulator):
        model_or_simulator = SimulatorPseudoModel(model) if use_simulator else model
        predicted_Cp, predicted_validities = get_transformed_Cp_predictions_and_validities(tf.convert_to_tensor(parameters, dtype=tf.float32), model_or_simulator, deformer_cls, joint_Dt1, joint_n)
        predicted_Cp = predicted_Cp.numpy()
        predicted_validities = predicted_validities.numpy()
        predicted_Cp[np.logical_not(predicted_validities)] = np.nan
        plt.pcolormesh(joint_Dt1, joint_n, predicted_Cp, vmin=1.255, vmax=1.290, cmap='magma')
        # plt.pcolormesh(joint_Dt1, joint_n, cropped_experimental_Cp, vmin=1.255, vmax=1.290, cmap='magma')
        plt.xlim(plot_x_range)
        plt.ylim(plot_y_range)
        plt.title(title)
        plt.show()

    main_optimiser = tf.keras.optimizers.Adam(learning_rate=0.01)

    for iteration in range(5000):

        with tf.GradientTape() as tape:
            predicted_Cp, validities = get_transformed_Cp_predictions_and_validities(parameters, model, deformer_cls, joint_Dt1, joint_n)
            loss = get_multiscale_grad_loss(predicted_Cp, cropped_experimental_Cp, validities, 'grad-zncc', verbose=False)

        gradient = tape.gradient(loss, parameters)
        gradient *= [1] * 4 + [1] * (len(min_parameters) - len(lower_CpCp) - 8) + [0] * len(lower_CpCp) + [1] * 4  # zero out gradients for CpCp mapping
        main_optimiser.apply_gradients([(gradient, parameters)])

        if iteration % 10 == 0:
            print(f'{iteration}: loss = {loss.numpy():.3f}')
        if iteration % 100 == 0:
            print(f'  predicted physical parameters: g2 = {parameters[-4].numpy():.1f}, g3 = {parameters[-3].numpy():.1f}, g4 = {parameters[-2].numpy():.1f}, Dt2 = {parameters[-1].numpy():.1f}')
            # plot_for_parameters(parameters, f'iteration {iteration}', use_simulator=False)

    # The following are the best parameters found by the above optimisation loop; comment the loop and uncomment
    # this to redo only the downstream analysis
    # parameters.assign([
    #     0.071690045, -5.0651364, -0.07169045, -5.602637, 0.34644172, -0.0020267458, 0.0125141265, -0.027177142,
    #      -0.10976354, 5.5598207, -0.24413636, 0.20054927, 6.9074306, 0.05, 0.45, 1.25, 1.45, 0.0, 0.0, 0.0, 0.0, 0.0,
    #      0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -15.509657, -312.93997, 132.32892, 3.0014672
    # ])

    print(f'final physical parameters: g2 = {parameters[-4].numpy():.1f}, g3 = {parameters[-3].numpy():.1f}, g4 = {parameters[-2].numpy():.1f}, Dt2 = {parameters[-1].numpy():.1f}')
    print('full parameters:', '[' + ', '.join(map(str, parameters.numpy())) + ']')
    plot_for_parameters(parameters, 'final (predicted before CpCp refinement)', use_simulator=False)

    plot_sensitivities(parameters.numpy(), g234Dt2_range, model, deformer_cls, joint_Dt1, joint_n, cropped_experimental_Cp, 'grad-zncc', plot_for_parameters)

    def fit_cpcp_with_mae():

        # Fit beta, gamma, and CpCp mapping, but leaving the original fitted parameters unchanged
        # This new set is used for visualisation of the fit (comparing to experimental), but should not be used for the sensitivity analysis

        parameters_with_cpcp = tf.Variable(initial_value=parameters, trainable=True)
        cpcp_optimiser = tf.keras.optimizers.Adam(learning_rate=0.01)  # fresh optimiser to ensure no momentum-induced changes to other parameters

        for _ in tqdm(range(200), 'refining CpCp'):

            with tf.GradientTape() as tape:
                predicted_Cp, validities = get_transformed_Cp_predictions_and_validities(parameters_with_cpcp, model, deformer_cls, joint_Dt1, joint_n)
                loss = get_multiscale_grad_loss(predicted_Cp, cropped_experimental_Cp, validities, 'val-mae', verbose=False)

            gradient = tape.gradient(loss, parameters_with_cpcp)
            gradient *= [1] * 4 + [0] * (len(min_parameters) - len(lower_CpCp) - 8) + [1] * len(lower_CpCp) + [0]  * 4  # zero out all gradients other than betas, gammas, and CpCp
            cpcp_optimiser.apply_gradients([(gradient, parameters_with_cpcp)])

        plot_for_parameters(parameters_with_cpcp, 'final (predicted after CpCp refinement)', use_simulator=False)
        plot_for_parameters(parameters_with_cpcp, 'final (simulated after CpCp refinement)', use_simulator=True)

    fit_cpcp_with_mae()


def fit_to_experimental_Cp(args, model, experimental_Dt1, experimental_n, experimental_Cp, g234Dt2_range):

    # for now, we use a subset of the 'experimental' grid as the 'fitting' grid; however, could instead use an arbitrary grid and resample the experimental data
    experimental_D1_in_range = np.nonzero(np.all(np.logical_and(experimental_Dt1 >= -args.Dt1_max, experimental_Dt1 <= args.Dt1_max), axis=1))[0]
    # experimental_n_in_range = np.nonzero(np.all(np.logical_and(experimental_n >= args.n_min, experimental_n <= 0), axis=0))[0]
    experimental_n_in_range = np.nonzero(np.all(np.logical_and(experimental_n >= args.n_min, experimental_n <= args.n_max), axis=0))[0]
    joint_Dt1 = tf.constant(experimental_Dt1[experimental_D1_in_range.min() : experimental_D1_in_range.max(), experimental_n_in_range.min() : experimental_n_in_range.max()].astype(np.float32))
    joint_n = tf.constant(experimental_n[experimental_D1_in_range.min() : experimental_D1_in_range.max(), experimental_n_in_range.min() : experimental_n_in_range.max()].astype(np.float32))
    cropped_experimental_Cp = experimental_Cp[experimental_D1_in_range.min() : experimental_D1_in_range.max(), experimental_n_in_range.min() : experimental_n_in_range.max()].astype(np.float32)
    cropped_experimental_Cp = preprocess_experimental(cropped_experimental_Cp)
    def downsample(x):
        factor = 2
        return tf.squeeze(tf.nn.avg_pool2d(x[None, ..., None], ksize=factor, strides=factor, padding='VALID'), axis=[0, -1])
    joint_Dt1 = downsample(joint_Dt1)
    joint_n = downsample(joint_n)
    cropped_experimental_Cp = downsample(cropped_experimental_Cp.astype(np.float32))

    best_parameters = fit_to_Cp(model, joint_Dt1, joint_n, cropped_experimental_Cp, get_d9_parameters(), g234Dt2_range, restarts=50, plot_x_range=[experimental_Dt1.min(), experimental_Dt1.max()], plot_y_range=[experimental_n.min(), experimental_n.max()])

    # plot_simulated_data_near_parameters(
    #     args,
    #     joint_Dt1, joint_n,
    #     best_parameters[-4:],
    #     [experimental_Dt1.min(), experimental_Dt1.max()],
    #     [experimental_n.min(), experimental_n.max()],
    #     'best fit'
    # )

    # simulate_and_plot_on_grid(
    #     best_parameters[-4:],
    #     joint_Dt1, joint_n,
    #     [experimental_Dt1.min(), experimental_Dt1.max()],
    #     [experimental_n.min(), experimental_n.max()],
    #     'best fit'
    # )


def fit_to_simulated_Cp(args, model, sim_data):

    # The following match the filters we perform when preprocessing train/val data
    sim_data = sim_data[np.logical_not(np.all(np.abs(sim_data[:, 2:]) < 1.e-2, axis=1))]
    sim_data = sim_data[np.logical_and(args.Dt1_min <= sim_data[:, 0], sim_data[:, 0] <= args.Dt1_max)]
    sim_data = sim_data[np.logical_and(args.n_min <= sim_data[:, 2], sim_data[:, 2] <= args.n_max)]

    D1, n = np.meshgrid(np.linspace(-args.Dt1_max, args.Dt1_max, 250), np.linspace(args.n_min, args.n_max, 150), indexing='ij')
    interpolator = scipy.interpolate.NearestNDInterpolator(sim_data[:, ::2], sim_data[:, 3])
    sim_data_gridded = interpolator(np.abs(D1), n)

    return fit_to_Cp(model, tf.convert_to_tensor(D1.astype(np.float32)), tf.convert_to_tensor(n.astype(np.float32)), 1 / (1 + tf.convert_to_tensor(sim_data_gridded.astype(np.float32))), None, restarts=10, plot_x_range=[-args.Dt1_max, args.Dt1_max], plot_y_range=[args.n_min, args.n_max])


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--data-path', type=str, default='./data/ABA/2021-11-15')
    parser.add_argument('--checkpoint-path', type=str, default='./output/ABA/2021-11-21_no-steps-per-epoch')
    parser.add_argument('--d9-path', type=str, default='./ABAtrilayer/data/d9.mat')
    parser.add_argument('--seed', type=int, default=0)
    parser.add_argument('--val-fraction', type=float, default=0.1)
    parser.add_argument('--test-fraction', type=float, default=0.15)
    parser.add_argument('--n-as-output', action='store_true')
    parser.add_argument('--Dt1-min', type=float, default=0.)  # these define the range the model was trained on (and hence gives valid output for!)
    parser.add_argument('--Dt1-max', type=float, default=75.)
    parser.add_argument('--n-min', type=float, default=-4.)
    parser.add_argument('--n-max', type=float, default=4.)
    parser.add_argument('--eta-min', type=float, default=5.e1)
    parser.add_argument('--eta-max', type=float, default=5.e3)
    args = parser.parse_args()

    args.train_batch_size = args.test_batch_size = 1  # required for dataset, but we always treat a whole plot as a batch

    if len(tf.config.get_visible_devices('GPU')) > 0:
        tf.config.experimental.set_memory_growth(tf.config.get_visible_devices('GPU')[0], True)

    np.random.seed(args.seed)
    tf.random.set_seed(args.seed)

    D1, n, C_p = load_d9_data(args.d9_path)
    # plot_d9_data(args, D1, n, C_p, limit_range=False)
    plot_d9_data(args, D1, n, C_p, limit_range=True)
    # plot_d9_data(args, D1, n, C_p, limit_range=False)
    # plot_d9_data(args, D1, n, C_p, limit_range=False, flip=True)

    # plot_simulated_data_near_experimental_parameters(args, D1, n)

    dataset = data.DatasetV2(args, plots_only=True)

    min_inputs = np.asarray([dataset.g2_range[0], dataset.g3_range[0], dataset.g4_range[0], dataset.Dt2_range[0], args.Dt1_min, args.n_min, args.eta_min])
    max_inputs = np.asarray([dataset.g2_range[1], dataset.g3_range[1], dataset.g4_range[1], dataset.Dt2_range[1], args.Dt1_max, args.n_max, args.eta_max])
    model = regressor.build(
        input_count=7, output_count=2 if args.n_as_output else 1,
        hidden_width=512, hidden_depth=5,
        activation=tf.nn.elu,
        train_input_min=min_inputs, train_input_max=max_inputs,
        fourier_scales=4
    )
    model.load_weights(args.checkpoint_path + '/ckpt').expect_partial()
    assert tf.reduce_all(model.Dt1_range == [args.Dt1_min, args.Dt1_max])
    assert tf.reduce_all(model.n_range == [args.n_min, args.n_max])
    model.eta_range = [args.eta_min, args.eta_max]

    plot_regressed_data_for_experimental_parameters(args, model, D1, n)
    fit_to_experimental_Cp(args, model, D1, n, C_p, [min_inputs[:4], max_inputs[:4]])


if __name__ == '__main__':

    main()

