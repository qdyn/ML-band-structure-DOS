
# Deep learning extraction of band structure parameters from density of states: a case study on trilayer graphene

_Paul Henderson, Areg Ghazaryan, Alexander A. Zibrov, Andrea F. Young and Maksym Serbyn_

This repository includes code for training and evaluating the deep neural network (DNN) model proposed in the above paper. We also provide a pretrained checkpoint, to allow evaluation of the model without retraining.

The code is implemented in Python, and depends on TensorFlow. A modern Nvidia GPU is strongly recommended if retraining the model.

## Preparation

 To create and activate a suitable conda environment, run 
 ```
 conda env create --name graphene-ml --file environment.yml
 conda activate graphene-ml
 ```

To build the numerical simulator (used during evaluation), run
```
cd simulator
CPATH=$CONDA_PREFIX/include LIBRARY_PATH=$CONDA_PREFIX/lib make
export LD_LIBRARY_PATH=$CONDA_PREFIX/lib:$LD_LIBRARY_PATH
```
If you already have a version of Intel MKL installed at system level, you can simply use `make` without modifying the environment.
Note that the simulator is hard-coded to use the 'high resolution' settings described in the paper. To change to 'low resolution', 
modify `simulator/constants.h` as noted in the comments there. For optimal performance you may also want to modify `nCore`.

## Training

To train the model from scratch, you will first need to generate or obtain the simulated training data.
For training, use
```
python train.py \
  --data-path YOUR-PATH-TO-SIMULATED-DATA \
  --checkpoint-path YOUR-PATH-TO-SAVE-MODEL-CHECKPOINTS
```
with appropriate paths substituted.
When training the model for the first time, you need to preprocess the simulated data, by setting `force_preprocess=True` at `data.py` line 46.
If using a GPU with very limited memory, you may need to pass `--test-batch-size` with a value smaller than the default (1000000), and or `--train-batch-size` less than 512.

Once completed, the training process will also run an evaluation of the forward accuracy. Alternatively, this can be run from the trained model as described below.

## Evaluation (forward and inverse)

We assume you either trained a model from scratch, or downloaded our pretrained checkpoint, to folder `YOUR-CHECKPOINT-FOLDER`.
To evaluate accuracy on the forward problem of predicting the DOS from the band structure parameters, use
```
python train.py \
  --data-path YOUR-PATH-TO-SIMULATED-DATA \
  --checkpoint-path YOUR-CHECKPOINT-FOLDER \
  --resume
```
If using a GPU with very limited memory, you may need to pass `--test-batch-size` with a value smaller than the default (1000000).
Numerical results will be printed to the terminal, and visual comparisons will be saved to `YOUR-CHECKPOINT-FOLDER/comparisons`.

To evaluate accuracy on the inverse problem of predicting the band structure parameters from a plot of the DOS, use
```
python inverter.py \
  --data-path YOUR-PATH-TO-SIMULATED-DATA \
  --checkpoint-path YOUR-CHECKPOINT-FOLDER
```
This defaults to evaluating for 100 sets of parameters; to modify this add `--count YOUR-COUNT`.
Numerical results will be printed to the terminal, and visual comparisons will be saved to `YOUR-CHECKPOINT-FOLDER/inversions`.

## Evaluation on experimental data

To reproduce the evaluation on experimental data, including sensitivity analysis, use
```
python experimental_data.py \
  --data-path YOUR-PATH-TO-SIMULATED-DATA \
  --checkpoint-path YOUR-CHECKPOINT-FOLDER \
  --d9-path PATH-TO-EXPERIMENTAL-DATASET
```
Numerical results will be printed to the terminal, and visual results will be displayed interactively with Matplotlib.
To repeat the downstream evaluation without recomputing the fit, see the comment at line 663 of `experimental_data.py`.
